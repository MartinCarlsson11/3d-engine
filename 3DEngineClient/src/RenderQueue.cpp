#include "RenderQueue.h"

std::map<unsigned int, RenderInformation> RenderQueue::renderQueue = std::map<unsigned int, RenderInformation>();
unsigned int RenderQueue::renderCallCount = 0;

unsigned int RenderQueue::AddDrawCall(RenderInformation information){
	renderQueue[renderCallCount] = information;
	int ret = renderCallCount;
	renderCallCount++;
	return ret;
}

void RenderQueue::RemoveDrawCall(int index)
{
	renderQueue.erase(index);
}