#include "TestState.h"
#include "InputHandler.h"
#include "glm\glm.hpp"
#include "glm\gtx\euler_angles.hpp"
#include "glm\gtx\transform.hpp"
#include "glm\gtc\matrix_transform.hpp"
#include "Window.h"
#include "Serialization.h"

TestState::TestState(){
	frameCounter = 0;
	accepted = false;
	gameStateReceived = false;
	clientKeyState = 0;
	playerID = 0;

	networkManager = std::unique_ptr<NetworkManager>(new NetworkManager(_CLIENT));
	moveSystem = std::make_unique<MoveSystem>();
	renderSystem = std::make_unique<RenderSystem>();
	playerSystem = std::make_unique<PlayerSystem>();
	physicsSystem = std::make_unique<PhysicsSystem>();
	collisionSystem = std::make_unique<CollisionSystem>();

	entities.resize(MAXENTITIES );

	std::vector<char> joinPacket;
	joinPacket.push_back(PacketType::JOIN);
	Packet packet = Packet(joinPacket, Address(127, 0, 0, 1, SERVER_PORT));	
	networkManager->AddPacket(packet);
}

bool TestState::Update() {
	networkManager->Update(this);

	if (accepted)
	{
		if (gameStateReceived)
		{
			HandleInputs();

			playerSystem->Update(entities, std::vector<Entity>());
			moveSystem->Update(entities);

			collisionSystem->Update(entities, std::vector<int>());

			if (InputHandler::GetKeyDown(KEY_7))
			{
				std::cout << "Current position: " << entities[playerID]->pX <<", "<< entities[playerID]->pY << ", " << entities[playerID]->pZ << std::endl;
			}
			renderSystem->Update(entities, playerID);

			frameCounter++;
		}
	}



	if (InputHandler::GetKey(VK_ESCAPE))
	{
		std::vector<char> data;
		data.push_back(PacketType::DISCONNECT);
		Serialize(playerID, data);
		Packet packet = Packet(data, Address(127, 0, 0, 1, SERVER_PORT));
		networkManager->AddPacket(packet);

		networkManager->Update(this);
		return false;
	}
	return true;
}

void TestState::Input(std::vector<char> inputs, Address sender)
{
	if (inputs[0] == PacketType::GAMESTATE)
	{
		if (gameStateReceived == false)
		{
			gameStateReceived = true;
		}

		UnSerialize(frameCounter, inputs.data(), 1);

		unsigned int count = 0;
		UnSerialize(count, inputs.data(), 5);

		unsigned int mask = 0;
		UnSerialize(mask, inputs.data(), 9);

		unsigned int parseCounter = 13;
		

		for (size_t i = 0; i < count; i++)
		{
			unsigned int id = 0;

			for (int i = 0; i < MAXENTITIES; i++)
			{
				if ((mask >> i) & 1)
				{
					id = i;
					mask &= ~(1 << id);
					break;
				}
			}

			if (entities[id] == nullptr)
			{
				entities[id] = std::make_shared<Entity>();
			}

			UnSerialize(entities[id]->componentIndex, inputs.data(), parseCounter);
			parseCounter += 4;

			UnSerialize(entities[id]->pX, inputs.data(), parseCounter);
			parseCounter += 4;
			UnSerialize(entities[id]->pY, inputs.data(), parseCounter);
			parseCounter += 4;
			UnSerialize(entities[id]->pZ, inputs.data(), parseCounter);
			parseCounter += 4;

			UnSerialize(entities[id]->rX, inputs.data(), parseCounter);
			parseCounter += 4;
			UnSerialize(entities[id]->rY, inputs.data(), parseCounter);
			parseCounter += 4;
			UnSerialize(entities[id]->rZ, inputs.data(), parseCounter);
			parseCounter += 4;

			UnSerialize(entities[id]->sX, inputs.data(), parseCounter);
			parseCounter += 4;
			UnSerialize(entities[id]->sY, inputs.data(), parseCounter);
			parseCounter += 4;
			UnSerialize(entities[id]->sZ, inputs.data(), parseCounter);
			parseCounter += 4;

			if (entities[id]->componentIndex >> Components::Move & 1)
			{	
				UnSerialize(entities[id]->dX, inputs.data(), parseCounter);
				parseCounter += 4;
				UnSerialize(entities[id]->dY, inputs.data(), parseCounter);			
				parseCounter += 4;
				UnSerialize(entities[id]->dZ, inputs.data(), parseCounter);			
				parseCounter += 4;
				UnSerialize(entities[id]->velocity, inputs.data(), parseCounter);			
				parseCounter += 4;
			}
			if (entities[id]->componentIndex >> Components::Player & 1)
			{
				UnSerialize(entities[id]->keyStates, inputs.data(), parseCounter);
				parseCounter += 4;
				UnSerialize(entities[id]->hasFired, inputs.data(), parseCounter);
				parseCounter += 4;
			}
			if (entities[id]->componentIndex >> Components::Renderable & 1)
			{
				UnSerialize(entities[id]->meshID, inputs.data(), parseCounter);
				parseCounter += 4;
			}

			if (entities[id]->componentIndex >> Components::Light & 1)
			{
			}
			if (entities[id]->componentIndex >> Components::Collidable & 1)
			{
				UnSerialize(entities[id]->bcX, inputs.data(), parseCounter);
				parseCounter += 4;
				UnSerialize(entities[id]->bcY, inputs.data(), parseCounter);
				parseCounter += 4;
				UnSerialize(entities[id]->bcZ, inputs.data(), parseCounter);
				parseCounter += 4;
				UnSerialize(entities[id]->bcXOffset, inputs.data(), parseCounter);
				parseCounter += 4;
				UnSerialize(entities[id]->bcYOffset, inputs.data(), parseCounter);
				parseCounter += 4;
				UnSerialize(entities[id]->bcZOffset, inputs.data(), parseCounter);
				parseCounter += 4;
			}
			if (entities[id]->componentIndex >> Components::Ability_Fireball & 1)
			{
				UnSerialize(entities[id]->bounceCount, inputs.data(), parseCounter);
				parseCounter += 4;
			}
		}
	}
	if(inputs[0] == PacketType::ACCEPT)
	{
		accepted = true;

		char PID[] = { inputs[1], inputs[2], inputs[3], inputs[4]};
		memcpy(&playerID, &PID, sizeof(playerID));
	}
	if (inputs[0] == PacketType::DENY)
	{
		accepted = false;
	}
	if (inputs[0] == PacketType::REMOVEENTITY)
	{
		int frame = 0;
		UnSerialize(frame, inputs.data(), 1);

		unsigned int id	= 0;
		UnSerialize(id, inputs.data(), 5);
		if (id == playerID)
		{
			playerID = -1;
		}
		if (entities[id]->componentIndex >> Components::Renderable & 1)
		{
			if (entities[id]->componentIndex >> Components::Light & 1)
			{
				renderSystem->RemoveEntity(id, entities[id]->lightId, entities[id]->renderId);
			}
			else
			{
				renderSystem->RemoveEntity(id, -1, entities[id]->renderId);
			}
		}
		else if (entities[id]->componentIndex >> Components::Light & 1)
		{
			renderSystem->RemoveEntity(id, entities[id]->lightId, -1);
		}
		entities[id].reset();
	}
	if (inputs[0] == PacketType::OBSERVER)
	{
		playerID = -1;
	}
}

void TestState::HandleInputs()
{
	if (InputHandler::GetKeyDown(KEY_W))
	{
		clientKeyState |= 1 << PLAYER_KEY_W;
	}
	else if (InputHandler::GetKeyUp(KEY_W))
	{
		clientKeyState &= ~(1 << PLAYER_KEY_W);
	}
	if (InputHandler::GetKeyDown(KEY_A))
	{
		clientKeyState |= 1 << PLAYER_KEY_A;
	}
	else if (InputHandler::GetKeyUp(KEY_A))
	{
		clientKeyState &= ~(1 << PLAYER_KEY_A);
	}
	if (InputHandler::GetKeyDown(KEY_S))
	{
		clientKeyState |= 1 << PLAYER_KEY_S;
	}
	else if (InputHandler::GetKeyUp(KEY_S))
	{
		clientKeyState &= ~(1 << PLAYER_KEY_S);
	}
	if (InputHandler::GetKeyDown(KEY_D))
	{
		clientKeyState |= 1 << PLAYER_KEY_D;
	}
	else if (InputHandler::GetKeyUp(KEY_D))
	{
		clientKeyState &= ~(1 << PLAYER_KEY_D);
	}
	if (InputHandler::GetKeyDown(KEY_1))
	{
		clientKeyState |= 1 << PLAYER_KEY_1;
	}
	else if (InputHandler::GetKeyUp(KEY_1))
	{
		clientKeyState &= ~(1 << PLAYER_KEY_1);
	}
	if (InputHandler::GetKeyDown(KEY_2))
	{
		clientKeyState |= 1 << PLAYER_KEY_2;
	}
	else if (InputHandler::GetKeyUp(KEY_2))
	{
		clientKeyState &= ~(1 << PLAYER_KEY_2);
	}
	if (InputHandler::GetKeyDown(KEY_3))
	{
		clientKeyState |= 1 << PLAYER_KEY_3;
	}
	else if (InputHandler::GetKeyUp(KEY_3))
	{
		clientKeyState &= ~(1 << PLAYER_KEY_3);
	}
	if (InputHandler::GetKeyDown(KEY_4))
	{
		clientKeyState |= 1 << PLAYER_KEY_4;
	}
	else if (InputHandler::GetKeyUp(KEY_4))
	{
		clientKeyState &= ~(1 << PLAYER_KEY_4);
	}
	if (InputHandler::GetKeyDown(KEY_5))
	{
		clientKeyState |= 1 << PLAYER_KEY_5;
	}
	else if (InputHandler::GetKeyUp(KEY_5))
	{
		clientKeyState &= ~(1 << PLAYER_KEY_5);
	}
	if (InputHandler::GetKeyDown(KEY_6))
	{
		clientKeyState |= 1 << PLAYER_KEY_6;
	}
	else if (InputHandler::GetKeyUp(KEY_6))
	{
		clientKeyState &= ~(1 << PLAYER_KEY_6);
	}
	if (InputHandler::GetKeyDown(VK_SPACE))
	{
		clientKeyState |= 1 << PLAYER_KEY_SPACE;
	}
	else if (InputHandler::GetKeyUp(VK_SPACE))
	{
		clientKeyState &= ~(1 << PLAYER_KEY_SPACE);
	}
	if (InputHandler::GetKeyDown(KEY_Q))
	{
		clientKeyState |= 1 << PLAYER_KEY_Q;
	}
	else if (InputHandler::GetKeyUp(KEY_Q))
	{
		clientKeyState &= ~(1 << PLAYER_KEY_Q);
	}
	if (InputHandler::GetKeyDown(KEY_E))
	{
		clientKeyState |= 1 << PLAYER_KEY_E;
	}
	else if (InputHandler::GetKeyUp(KEY_E))
	{
		clientKeyState &= ~(1 << PLAYER_KEY_E);
	}

	if (InputHandler::GetKeyDown(VK_LBUTTON))
	{
		clientKeyState |= 1 << PLAYER_LMOUSE;
	}
	else if (InputHandler::GetKeyUp(VK_LBUTTON))
	{
		clientKeyState &= ~(1 << PLAYER_LMOUSE);
	}
	if (playerID >= 0)
	{
		if (entities[playerID])
		{
			entities[playerID]->keyStates = clientKeyState;
		}
	}
	std::vector<char> inputData;
	inputData.push_back(PacketType::INPUT);

	Serialize(playerID, inputData);
	Serialize(frameCounter, inputData);
	Serialize(clientKeyState, inputData);

	Packet packet = Packet(inputData, Address(127, 0, 0, 1, SERVER_PORT));
	networkManager->AddPacket(packet);
}

IState * TestState::NextState() {
	return nullptr;
}