#include "Camera.h"
#include "glm\gtx\transform.hpp"
#include "glm\gtc\constants.hpp"
#include "glm\gtx\euler_angles.hpp"

#include <iostream>

Camera::Camera()
	:position(0.0f, 0.0f, 0.0f),
	targetPos(0.0f, 0.0f, 0.0f),
	up(0.0f, 1.0f, 0.0f),
	look(0.0f, 0.0f, 0.0f),
	right(0.0f, 0.0f, 0.0f),
	worldUp(0.0f, 1.0f, 0.0f),
	fov(45.0f),
	pitch(0.0f),
	yaw(glm::pi<float>())
{

}

glm::mat4 Camera::GetViewMatrix() const
{
	return glm::lookAt(position, targetPos, up);
}

const glm::vec3 & Camera::GetLook() const
{
	return look;
}

const glm::vec3 & Camera::GetRight() const
{
	return right;
}

const glm::vec3 & Camera::GetUp() const
{
	return up;
}

glm::vec3 Camera::GetPosition() const
{
	return position;
}


FPSCamera::FPSCamera(glm::vec3 position, float yaw, float pitch)
{
	this->position = position;
	this->yaw = yaw;
	this->pitch = pitch;
}

void FPSCamera::SetPosition(const glm::vec3 & position)
{
	this->position = position;
}

void FPSCamera::SetRotation(float yaw)
{
	this->yaw = glm::radians(yaw);
	UpdateCameraVectors();
}

void FPSCamera::Rotate(float yaw, float pitch)
{
	this->yaw += glm::radians(yaw);
	this->pitch += glm::radians(pitch);

	this->pitch = glm::clamp(this->pitch, -glm::pi<float>() / 2.0f +0.1f, glm::pi<float>() / 2.0f - 0.1f);
	UpdateCameraVectors();
}	

void FPSCamera::Move(const glm::vec3 & offsetpos)
{
	position += offsetpos;
	UpdateCameraVectors();
}

void FPSCamera::UpdateCameraVectors()
{
	glm::vec3 lookx;
	lookx.x = cosf(pitch) * sinf(yaw);
	lookx.y = sinf(pitch);
	lookx.z = cosf(pitch) * cosf(yaw);

	look = glm::normalize(lookx);
	right = glm::normalize(glm::cross(look, worldUp));
	up = glm::normalize(glm::cross(right, look));

	targetPos = position + look;
}

glm::mat4 LookAt(glm::vec3 position, glm::vec3 target, glm::vec3 up)
{
	glm::mat4 lookat;
	return lookat;
}

OrbitCamera::OrbitCamera()
	: radius(10.0f)
{
}

void OrbitCamera::SetPosition(const glm::vec3 & position)
{
	this->position = position;
}

void OrbitCamera::SetLookAt(const glm::vec3 & target)
{
	targetPos = target;
}

void OrbitCamera::SetRadius(float radius)
{
	this->radius = radius;
}

void OrbitCamera::UpdateCameraVectors()
{
	position.x = targetPos.x + radius * cosf(pitch) * sinf(yaw);
	position.y = targetPos.y + radius * sinf(pitch);
	position.z = targetPos.z + radius * cosf(pitch) * cosf(yaw);
}
