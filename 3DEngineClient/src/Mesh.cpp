#include "Mesh.h"
#include <fstream>
#include <sstream>
#include <iostream>
#include <string>
#include <glm\gtx\transform.hpp>
#include <glm\gtx\quaternion.hpp>
#include <glm\gtx\rotate_vector.hpp>

Mesh::Mesh()
{
	vbo = 0;
	vao = 0;
}

Mesh::~Mesh()
{
	DeleteBuffers();
}


void Mesh::LoadMesh(FbxMesh* mesh)
{
	if ((vao != 0) && (vbo != 0))
	{
		DeleteBuffers();
	}

	std::vector<Vertex> tempVertices;

	int count = mesh->GetControlPointsCount();
	FbxNode* node = mesh->GetNode();
	FbxVector4* verts = mesh->GetControlPoints();

	FbxAMatrix matrixGeo;
	if (node->GetNodeAttribute())
	{
		const FbxVector4 translation = node->GetGeometricTranslation(FbxNode::eSourcePivot);
		const FbxVector4 rotation = node->GetGeometricRotation(FbxNode::eSourcePivot);
		const FbxVector4 scale = node->GetGeometricScaling(FbxNode::eSourcePivot);
		matrixGeo.SetT(translation);
		matrixGeo.SetR(rotation);
		matrixGeo.SetS(scale);	
	}

	FbxAMatrix globalMatrix = node->EvaluateGlobalTransform();
	FbxAMatrix matrix = globalMatrix*matrixGeo;
	for (int i = 0; i < count; i++)
	{
		FbxVector4 result = matrix.MultT(verts[i]);

		Vertex vertexPos;
		vertexPos.position.x = (float)result.mData[0];
		vertexPos.position.y = (float)result.mData[1];
		vertexPos.position.z = (float)result.mData[2];

		tempVertices.push_back(vertexPos);
	}


	int deformerCount = mesh->GetDeformerCount();
	for (int i = 0; i < deformerCount; i++)
	{
		FbxSkin* skin = reinterpret_cast<FbxSkin*>(mesh->GetDeformer(i));
		int clusterCount = skin->GetClusterCount();
		for (int cI = 0; cI < clusterCount; cI++)
		{
			FbxCluster* cluster = skin->GetCluster(cI);
			int indiceCount = cluster->GetControlPointIndicesCount();
			int* indices = cluster->GetControlPointIndices();
			for (int vI = 0; vI < indiceCount; vI++)
			{
				if (tempVertices[indices[vI]].weights[0] == 0.0)
				{
					tempVertices[indices[vI]].weights[0] = cluster->GetControlPointWeights()[vI];
					tempVertices[indices[vI]].boneIds[0] = cI;
				}	
				else if (tempVertices[indices[vI]].weights[1] == 0.0)
				{

					tempVertices[indices[vI]].weights[1] = cluster->GetControlPointWeights()[vI];
					tempVertices[indices[vI]].boneIds[1] = cI;
				}
				else if (tempVertices[indices[vI]].weights[2] == 0.0)
				{
					tempVertices[indices[vI]].weights[2] = cluster->GetControlPointWeights()[vI];
					tempVertices[indices[vI]].boneIds[2] = cI;
				}
				else if (tempVertices[indices[vI]].weights[3] == 0.0)
				{
					tempVertices[indices[vI]].weights[3] = cluster->GetControlPointWeights()[vI];
					tempVertices[indices[vI]].boneIds[3] = cI;
				}
			}
		}
	}
	
	FbxLayerElementArrayTemplate<FbxVector2>* uvs;
	mesh->GetTextureUV(&uvs);

	count = mesh->GetPolygonCount();
	for (int i = 0; i < count; i++)
	{
		int vertexCount = mesh->GetPolygonSize(i);
		int vertexIndex = mesh->GetPolygonVertexIndex(i);
		int* verticesIndex = &mesh->GetPolygonVertices()[vertexIndex];

		if (vertexCount == 4)
		{
			FbxVector4 normal;

			int uvIndex1 = mesh->GetTextureUVIndex(i, 0);
			int uvIndex2 = mesh->GetTextureUVIndex(i, 1);
			int uvIndex3 = mesh->GetTextureUVIndex(i, 2);
			int uvIndex4 = mesh->GetTextureUVIndex(i, 3);
			FbxVector2 uv1 = uvs->GetAt(uvIndex1);
			FbxVector2 uv2 = uvs->GetAt(uvIndex2);
			FbxVector2 uv3 = uvs->GetAt(uvIndex3);
			FbxVector2 uv4 = uvs->GetAt(uvIndex4);

			tempVertices[verticesIndex[0]].texCoords = glm::vec2(uv1.mData[0], uv1.mData[1]);
			bool success = mesh->GetPolygonVertexNormal(i, 0, normal);
			tempVertices[verticesIndex[0]].normal = glm::vec3(normal.mData[0], normal.mData[1], normal.mData[2]);
			vertices.push_back(tempVertices[verticesIndex[0]]);

			tempVertices[verticesIndex[1]].texCoords = glm::vec2(uv2.mData[0], uv2.mData[1]);
			mesh->GetPolygonVertexNormal(i, 1, normal);
			tempVertices[verticesIndex[1]].normal = glm::vec3(normal.mData[0], normal.mData[1], normal.mData[2]);
			vertices.push_back(tempVertices[verticesIndex[1]]);

			tempVertices[verticesIndex[2]].texCoords = glm::vec2(uv3.mData[0], uv3.mData[1]);
			mesh->GetPolygonVertexNormal(i, 2, normal);
			tempVertices[verticesIndex[2]].normal = glm::vec3(normal.mData[0], normal.mData[1], normal.mData[2]);
			vertices.push_back(tempVertices[verticesIndex[2]]);

			tempVertices[verticesIndex[0]].texCoords = glm::vec2(uv1.mData[0], uv1.mData[1]);
			mesh->GetPolygonVertexNormal(i, 0, normal);
			tempVertices[verticesIndex[0]].normal = glm::vec3(normal.mData[0], normal.mData[1], normal.mData[2]);
			vertices.push_back(tempVertices[verticesIndex[0]]);

			tempVertices[verticesIndex[2]].texCoords = glm::vec2(uv3.mData[0], uv3.mData[1]);
			mesh->GetPolygonVertexNormal(i, 2, normal);
			tempVertices[verticesIndex[2]].normal = glm::vec3(normal.mData[0], normal.mData[1], normal.mData[2]);
			vertices.push_back(tempVertices[verticesIndex[2]]);

			tempVertices[verticesIndex[3]].texCoords = glm::vec2(uv4.mData[0], uv4.mData[1]);
			mesh->GetPolygonVertexNormal(i, 3, normal);
			tempVertices[verticesIndex[3]].normal = glm::vec3(normal.mData[0], normal.mData[1], normal.mData[2]);
			vertices.push_back(tempVertices[verticesIndex[3]]);
		}
		else if (vertexCount == 3)
		{
			FbxVector4 normal;

			int uvIndex1 = mesh->GetTextureUVIndex(i, 0);
			int uvIndex2 = mesh->GetTextureUVIndex(i, 1);
			int uvIndex3 = mesh->GetTextureUVIndex(i, 2);
			FbxVector2 uv1 = uvs->GetAt(uvIndex1);
			FbxVector2 uv2 = uvs->GetAt(uvIndex2);
			FbxVector2 uv3 = uvs->GetAt(uvIndex3);

			tempVertices[verticesIndex[0]].texCoords = glm::vec2(uv1.mData[0], uv1.mData[1]);
			mesh->GetPolygonVertexNormal(i, 0, normal);
			tempVertices[verticesIndex[0]].normal = glm::vec3(normal.mData[0], normal.mData[1], normal.mData[2]);
			vertices.push_back(tempVertices[verticesIndex[0]]);

			tempVertices[verticesIndex[1]].texCoords = glm::vec2(uv2.mData[0], uv2.mData[1]);
			mesh->GetPolygonVertexNormal(i, 1, normal);
			tempVertices[verticesIndex[1]].normal = glm::vec3(normal.mData[0], normal.mData[1], normal.mData[2]);
			vertices.push_back(tempVertices[verticesIndex[1]]);

			tempVertices[verticesIndex[2]].texCoords = glm::vec2(uv3.mData[0], uv3.mData[1]);
			mesh->GetPolygonVertexNormal(i, 2, normal);
			tempVertices[verticesIndex[2]].normal = glm::vec3(normal.mData[0], normal.mData[1], normal.mData[2]);
			vertices.push_back(tempVertices[verticesIndex[2]]);
		}
	}

	InitBuffers();
}

void Mesh::LoadMesh(const std::string & filename)
{
	if ((vao != 0) && (vbo != 0))
	{
		DeleteBuffers();
	}
	if (filename.find(".obj") != std::string::npos)
	{
		std::vector <unsigned int> vertexIndices, uvIndices, normalIndices;
		std::vector<glm::vec3> tempVertices;
		std::vector<glm::vec2> tempUVs;
		std::vector<glm::vec3> tempNormals;

		std::ifstream fin(filename, std::ios::in);

		if (!fin)
		{
			std::cout << "could not load obj file" << std::endl;
		}
		else
		{

			std::string lineBuffer;
			while (std::getline(fin, lineBuffer))
			{
				std::stringstream ss(lineBuffer);
				std::string cmd;
				ss >> cmd;

				if (cmd == "v")
				{
					glm::vec3 vertex;
					int dim = 0;
					while ((dim < 3) && (ss >> vertex[dim]))
					{
						dim++;
					}
					tempVertices.push_back(vertex);
				}
				else if (cmd == "vt")
				{
					glm::vec2 uv;
					int dim = 0;
					while ((dim < 2) && (ss >> uv[dim]))
					{
						dim++;
					}
					tempUVs.push_back(uv);
				}
				else if (cmd == "vn")
				{
					glm::vec3 n;
					int dim = 0;
					while ((dim < 3) && (ss >> n[dim]))
					{
						dim++;
					}
					n = glm::normalize(n);
					tempNormals.push_back(n);
				}
				else if (cmd == "f")
				{
					std::string faceData;
					int vertexIndex, uvIndex, normalIndex;

					while (ss >> faceData)
					{
						std::vector<std::string> data = Split(faceData, "/");
						if (data[0].size() > 0)
						{
							sscanf_s(data[0].c_str(), "%d", &vertexIndex);
							vertexIndices.push_back(vertexIndex);
						}
						if (data.size() >= 1)
						{
							if (data[1].size() > 0)
							{
								sscanf_s(data[1].c_str(), "%d", &uvIndex);
								uvIndices.push_back(uvIndex);
							}
						}
						if (data.size() >= 2)
						{
							if (data[2].size() > 0)
							{
								sscanf_s(data[2].c_str(), "%d", &normalIndex);
								normalIndices.push_back(normalIndex);
							}
						}
					}
				}
			}
			fin.close();

			for (unsigned int i = 0; i < vertexIndices.size(); i++)
			{
				Vertex meshVertex;
				if (tempVertices.size() > 0)
				{
					glm::vec3 vertex = tempVertices[vertexIndices[i] - 1];
					meshVertex.position = vertex;
				}
				if (tempUVs.size() > 0)
				{
					glm::vec2 uv = tempUVs[uvIndices[i] - 1];
					meshVertex.texCoords = uv;
				}
				if (tempNormals.size() > 0)
				{
					glm::vec3 normal = tempNormals[normalIndices[i] - 1];
					meshVertex.normal = normal;
				}
				vertices.push_back(meshVertex);
			}
			InitBuffers();

		}
	}
}

GLuint Mesh::GetVertexArray() const
{
	return vao;
}

int Mesh::GetVertexCount() const
{
	return (int)vertices.size();
}

int Mesh::GetBuffer() const
{
	return vbo;	
}

void Mesh::LoadMeshFromVertices(std::vector<Vertex> vertices)
{
	if ((vao != 0) && (vbo != 0))
	{
		DeleteBuffers();
	}
	this->vertices = vertices;
	InitBuffers();
}

std::vector<MeshBone> Mesh::GetBindPose() const
{
	return meshBones;
}

void Mesh::InitBuffers()
{
	glGenBuffers(1, &vbo);

	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(Vertex), &vertices[0], GL_STATIC_DRAW);

	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	//position
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), NULL);
	glEnableVertexAttribArray(0);

	//normals
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)(3 * sizeof(GLfloat)));
	glEnableVertexAttribArray(1);

	//tex coords
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)(6 * sizeof(GLfloat)));
	glEnableVertexAttribArray(2);

	//bone weights
	glVertexAttribPointer(3, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)(8* sizeof(GLfloat)));
	glEnableVertexAttribArray(3);

	//bone ids
	glVertexAttribIPointer(4, 4, GL_INT, sizeof(Vertex), (GLvoid*)(12 * sizeof(GLfloat)));
	glEnableVertexAttribArray(4);

	glBindVertexArray(0);
}

void Mesh::DeleteBuffers()
{
	glDeleteVertexArrays(1, &vao);
	glDeleteBuffers(1, &vbo);
}

std::vector<std::string> Mesh::Split(std::string input, std::string delimiter)
{
	std::vector<std::string> res;
	while(1)
	{
		int pos = (int)input.find(delimiter);
		if (pos == -1)
		{
			res.push_back(input);
			break;
		}
		res.push_back(input.substr(0, pos));
		input = input.substr(pos + 1, input.size() - pos - 1);
	}
	return res;
}
