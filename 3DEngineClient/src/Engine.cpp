#include "Engine.h"
#include <iostream>
#include "TestState.h"
bool Engine::isRunning = false;

Engine::Engine()
	:targetTime(1.0 / 60.0),
	timeStepAccumulator(0.0),
	frameTimeAccumulator(0.0),
	fps(0),
	lastTime(std::chrono::high_resolution_clock::now()) {
	window = std::make_unique<Window>(800, 600, "Engine");
	renderer = std::make_unique<Renderer>(window->window);
	stateMachine = std::make_unique<StateMachine>(new TestState());
	isRunning = true;
	stateMachine->Start();
}

Engine::~Engine() {
}

bool Engine::Update() {
	if (TimeStep()) {
		window->HandleEvents();

		if (!stateMachine->Update())
			isRunning = false;

		renderer->Update();
	}
	return isRunning;
}


bool Engine::TimeStep() {
	lastTime = currentTime;
	currentTime = std::chrono::high_resolution_clock::now();
	auto deltaTime = (currentTime - lastTime) / 1000000000.0;

	timeStepAccumulator += deltaTime.count();
	frameTimeAccumulator += deltaTime.count();

	if (frameTimeAccumulator > 1.0) {
		std::cout << "FPS: " << fps << std::endl;
		frameTimeAccumulator = 0;
		fps = 0;
	}

	if (timeStepAccumulator > targetTime) {
		timeStepAccumulator = 0;
		fps++;
		return true;
	}
	else {
		return false;
	}
}