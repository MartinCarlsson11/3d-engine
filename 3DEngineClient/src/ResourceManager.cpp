#include "ResourceManager.h"
#include "Entity.h"
#include "fbxsdk.h"
#include <functional>
#include <glm\gtx\transform.hpp>

void RecursiveNodeTraversal(FbxNode* node, std::vector<FbxNode*>& nodes);

ResourceManager::ResourceManager()
{
	fbxManager = FbxManager::Create();
	shaders.resize( MAXENTITIES );
	lightSystem = std::make_unique<LightSystem>();
	lightSystem->Init();
}

ResourceManager::~ResourceManager()
{
	shaders.clear();
	meshes.clear();
	textures.clear();
	animations.clear();
	fbxManager->Destroy();
	fbxManager = nullptr;
}

RenderInformation ResourceManager::Load(unsigned int id, unsigned int entityID)
{
	RenderInformation renderInfo;

	if (id == Meshes::CHARACTER1)
	{
		std::shared_ptr<Mesh> mesh = LoadMesh(Assets::assetPath + Assets::character1);
		renderInfo.texture = LoadTexture(Assets::assetPath + Assets::character1TextureDiffuse);
		renderInfo.vao = mesh->GetVertexArray();
		renderInfo.vbo = mesh->GetBuffer();
		renderInfo.indiceCount = mesh->GetVertexCount();
		shaders[entityID] = LoadShader(Assets::shaderPath + Assets::standardVert, Assets::shaderPath + Assets::standardFrag);

		renderInfo.shaderHandle = shaders[entityID]->GetHandle();

		shaders[entityID]->SetUniformBlock("lightData", lightSystem->GetLightUniformBlockBindPoint());

		shaders[entityID]->SetUniform("material.ambient", materialAmbience);
		shaders[entityID]->SetUniformSampler("material.diffuseMap",0);
		shaders[entityID]->SetUniform("material.specular", materialSpecular);
		shaders[entityID]->SetUniform("material.shininess", 128.0f);

	}
	else if (id == Meshes::CHARACTER2)
	{
		std::shared_ptr<Mesh> mesh = LoadMesh(Assets::assetPath + Assets::character2);
		renderInfo.texture = LoadTexture(Assets::assetPath + Assets::character2TextureDiffuse);
		renderInfo.vao = mesh->GetVertexArray();
		renderInfo.vbo = mesh->GetBuffer();
		renderInfo.indiceCount = mesh->GetVertexCount();
		shaders[entityID] = LoadShader(Assets::shaderPath + Assets::standardVert, Assets::shaderPath + Assets::standardFrag);

		renderInfo.shaderHandle = shaders[entityID]->GetHandle();
		shaders[entityID]->SetUniformBlock("lightData", lightSystem->GetLightUniformBlockBindPoint());
		shaders[entityID]->SetUniform("material.ambient", materialAmbience);
		shaders[entityID]->SetUniformSampler("material.diffuseMap", 0);
		shaders[entityID]->SetUniform("material.specular", materialSpecular);
		shaders[entityID]->SetUniform("material.shininess", 64.0f);
	}
	else if (id == Meshes::HEIGHTMAPFLOOR)
	{
		renderInfo.texture = LoadTexture(Assets::assetPath + Assets::heightMapTexture);
		
		HeightMap heightMap = HeightMap(Assets::assetPath + Assets::heightMapFloor);
		std::shared_ptr<Mesh> mesh = std::make_shared<Mesh>();
		mesh->LoadMeshFromVertices(heightMap.GetVertices());
		meshes["heightMapFloor"] = mesh;

		renderInfo.vao = mesh->GetVertexArray();
		renderInfo.vbo = mesh->GetBuffer();
		renderInfo.indiceCount = mesh->GetVertexCount();

		shaders[entityID] = LoadShader(Assets::shaderPath + Assets::standardVert, Assets::shaderPath + Assets::standardFrag);
		renderInfo.shaderHandle = shaders[entityID]->GetHandle();
		shaders[entityID]->SetUniformBlock("lightData", lightSystem->GetLightUniformBlockBindPoint());
		shaders[entityID]->SetUniform("material.ambient", materialAmbience);
		shaders[entityID]->SetUniformSampler("material.diffuseMap", 0);
		shaders[entityID]->SetUniform("material.specular", materialSpecular);
		shaders[entityID]->SetUniform("material.shininess", 1.0f);
		
	}
	else if (id == Meshes::HEIGHTMAPROOF)
	{
		renderInfo.texture = LoadTexture(Assets::assetPath + Assets::heightMapTexture);
		HeightMap heightMap = HeightMap(Assets::assetPath + Assets::heightMapRoof);
		std::shared_ptr<Mesh> mesh = std::make_shared<Mesh>();
		mesh->LoadMeshFromVertices(heightMap.GetVertices());
		meshes["heightMapRoof"] = mesh;

		renderInfo.vao = mesh->GetVertexArray();
		renderInfo.vbo = mesh->GetBuffer();
		renderInfo.indiceCount = mesh->GetVertexCount();

		shaders[entityID] = LoadShader(Assets::shaderPath + Assets::standardVert, Assets::shaderPath + Assets::standardFrag);
		renderInfo.shaderHandle = shaders[entityID]->GetHandle();
		shaders[entityID]->SetUniformBlock("lightData", lightSystem->GetLightUniformBlockBindPoint());
		shaders[entityID]->SetUniform("material.ambient", materialAmbience);
		shaders[entityID]->SetUniformSampler("material.diffuseMap",0);
		shaders[entityID]->SetUniform("material.specular", materialSpecular);
		shaders[entityID]->SetUniform("material.shininess", 1.0f);

	}
	else if (id == Meshes::NPC1)
	{
		std::shared_ptr<Mesh> mesh = LoadMesh(Assets::assetPath + Assets::npc1);
		renderInfo.texture = LoadTexture(Assets::assetPath + Assets::npc1TextureDiffuse);
		renderInfo.vao = mesh->GetVertexArray();
		renderInfo.vbo = mesh->GetBuffer();
		renderInfo.indiceCount = mesh->GetVertexCount();
		shaders[entityID] = LoadShader(Assets::shaderPath + Assets::standardVert, Assets::shaderPath + Assets::standardFrag);

		renderInfo.shaderHandle = shaders[entityID]->GetHandle();

		shaders[entityID]->SetUniformBlock("lightData", lightSystem->GetLightUniformBlockBindPoint());
		shaders[entityID]->SetUniform("material.ambient", materialAmbience);
		shaders[entityID]->SetUniformSampler("material.diffuseMap", 0);
		shaders[entityID]->SetUniform("material.specular", materialSpecular);
		shaders[entityID]->SetUniform("material.shininess", 64.0f);
	}
	else if (id == Meshes::FIREBALL)
	{
		std::shared_ptr<Mesh> mesh = LoadMesh(Assets::assetPath + Assets::fireball);
		renderInfo.texture = LoadTexture(Assets::assetPath + Assets::fireballDiffuse);
		renderInfo.vao = mesh->GetVertexArray();
		renderInfo.vbo = mesh->GetBuffer();
		renderInfo.indiceCount = mesh->GetVertexCount();
		shaders[entityID] = LoadShader(Assets::shaderPath + Assets::colorVert, Assets::shaderPath + Assets::colordFrag);

		renderInfo.shaderHandle = shaders[entityID]->GetHandle();
		shaders[entityID]->SetUniform("color", glm::vec3(156.f / 255.f, 42.f / 255.f, 0.f / 255.f));
	}
	return renderInfo;
}

void ResourceManager::UpdateShaders(std::vector<int> id, std::vector<glm::mat4> models, glm::mat4 view, glm::vec3 viewPos, glm::mat4 projection, std::vector<int> lightIds, std::vector<glm::vec3> lightPositions)
{
	lightSystem->Update(lightIds, lightPositions, viewPos);

	int counter = 0;
	for (auto models : models)
	{
		shaders[id[counter]]->SetUniform("model", models);
		shaders[id[counter]]->SetUniform("view", view);
		shaders[id[counter]]->SetUniform("projection", projection);
		counter++;
	}
}

std::shared_ptr<Mesh> ResourceManager::LoadMesh(std::string fileName)
{
	auto it = meshes.find(fileName);
	if (it != meshes.end())
	{
		return it->second;
	}
	else
	{
		std::shared_ptr<Mesh> mesh = std::make_shared<Mesh>();
		if (fileName.find(".obj") != std::string::npos)
		{
			mesh->LoadMesh(fileName);
			meshes[fileName] = mesh;
		}
		else if (fileName.find(".fbx") != std::string::npos)
		{
			FbxIOSettings* ios = FbxIOSettings::Create(fbxManager, IOSROOT);
			fbxManager->SetIOSettings(ios);

			FbxImporter* importer = FbxImporter::Create(fbxManager, "");

			if (!importer->Initialize(fileName.data(), -1, fbxManager->GetIOSettings()))
			{
				std::cout << "Importer failed to initialize" << std::endl;
			}

			FbxScene* scene = FbxScene::Create(fbxManager, "myScene");

			importer->Import(scene);
			importer->Destroy();

			FbxNode* rootNode = scene->GetRootNode();
			if (rootNode)
			{
				int childCount = rootNode->GetChildCount();
				for (int i = 0; i < childCount; i++)
				{
					FbxNode* childNode = rootNode->GetChild(i);

					FbxNodeAttribute::EType nodeAttribute = (childNode->GetNodeAttribute()->GetAttributeType());
					if (nodeAttribute == FbxNodeAttribute::eMesh)
					{
						FbxMesh* fbxMesh = (FbxMesh*)childNode->GetNodeAttribute();
						if (fbxMesh != nullptr)
						{
							mesh->LoadMesh(fbxMesh);
							meshes[fileName] = mesh;
						}
					}
					/*
					int poseCount = scene->GetPoseCount();
					for (int x = 0; x < poseCount; x++)
					{
						FbxPose* pose = scene->GetPose(i);
						pose->Find(childNode);
						if (pose->IsBindPose() || !pose->IsLocalMatrix(i))
						{
							FbxMatrix poseMatrix = pose->GetMatrix(i);
						}
					}*/
				}
			}
			rootNode->Destroy(true);
		}
	}
	return meshes[fileName];
}

std::shared_ptr<Shader> ResourceManager::LoadShader(std::string vertShader, std::string fragShader)
{
	std::shared_ptr<Shader> shader = std::make_shared<Shader>();
	shader->LoadShaders(vertShader.data(), fragShader.data());
	return shader;
}

std::shared_ptr<Animation> ResourceManager::LoadAnimation(std::string fileName)
{
	auto it = animations.find(fileName);
	if (it != animations.end())
	{
		return it->second;
	}
	else if(fileName.find(".fbx") != std::string::npos)
	{
		FbxIOSettings* ios = FbxIOSettings::Create(fbxManager, IOSROOT);
		fbxManager->SetIOSettings(ios);

		FbxImporter* importer = FbxImporter::Create(fbxManager, "");

		if (!importer->Initialize(fileName.data(), -1, fbxManager->GetIOSettings()))
		{
			std::cout << "Importer failed to initialize" << std::endl;
		}

		FbxScene* scene = FbxScene::Create(fbxManager, "myScene");

		importer->Import(scene);
		importer->Destroy();
		std::vector<FbxNode*> nodes;

		FbxNode* rootNode = scene->GetRootNode();
		if (rootNode)
		{
			std::shared_ptr<Animation> animation = std::make_shared<Animation>();
	//		RecursiveNodeTraversal(rootNode, nodes);
/*			
			//LoadAnimation();
			//Send relevant data to the animation

			FbxAnimEvaluator* animEvaluator = scene->GetAnimationEvaluator();
			int animStackCount = scene->GetMemberCount<FbxAnimStack>();
			for (int i = 0; i < animStackCount; i++)
			{
				FbxAnimStack* animStack = scene->GetMember<FbxAnimStack>(i);
				std::vector<Frame> frames;
				frames.resize(30);

				for (auto node : nodes)
				{
					FbxTakeInfo* takeInfo = scene->GetTakeInfo(animStack->GetName());
					FbxTime start = takeInfo->mLocalTimeSpan.GetStart();
					FbxTime end = takeInfo->mLocalTimeSpan.GetStop();
					
					for (FbxLongLong numberOfFrames = start.GetFrameCount(FbxTime::eFrames24); numberOfFrames <= end.GetFrameCount(FbxTime::eFrames24); ++numberOfFrames)
					{
						FbxTime currTime;
						currTime.SetFrame(i, FbxTime::eFrames24);

						FbxAMatrix currentTransformOffset = node->EvaluateGlobalTransform(currTime);
						Bone bone;
						bone.boneOffset = currentTransformOffset.Inverse();

						frames[numberOfFrames].bones.push_back(bone);
					}

				}
				animation->frames = frames;
				animations[fileName] = animation;
			}*/
		}
	}
	return animations[fileName];
}

int ResourceManager::AddLight(glm::vec3 position, glm::vec3 color)
{
	return lightSystem->AddLight(position, color);
}

void ResourceManager::RemoveLight(unsigned int id)
{
	lightSystem->RemoveLight(id);
}

GLuint ResourceManager::LoadTexture(std::string fileName)
{
	auto it = textures.find(fileName);
	if (it != textures.end())
	{
		return it->second->GetTextureHandle();
	}
	else
	{
		std::shared_ptr<Texture2D>texture = std::make_shared<Texture2D>();
		texture->LoadTexture(fileName.c_str());
		textures[fileName] = texture;
	}
	return textures[fileName]->GetTextureHandle();
}

void RecursiveNodeTraversal(FbxNode* node, std::vector<FbxNode*> &nodes)
{
	int count = node->GetChildCount();
	for (int i = 0; i < count; i++)
	{
		FbxNode* childNode = node->GetChild(i);
		if (childNode)
		{
			nodes.push_back(childNode);
		}
		RecursiveNodeTraversal(childNode, nodes);
	}
}