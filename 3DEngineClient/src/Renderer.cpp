#include "Renderer.h"
#include "RenderQueue.h"
#include "Window.h"
#include <iostream>
#include "InputHandler.h"

Renderer::Renderer(HWND window)
: wireFrame(false)
{
	hdc = GetDC(window);

	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_MULTISAMPLE_ARB);
	glEnable(GL_SCISSOR_TEST);

	//glEnable(GL_CULL_FACE);
	//glCullFace(GL_CCW);
}

void Renderer::Update(){

	if (InputHandler::GetKeyDown(KEY_0))
		wireFrame = !wireFrame;
	if (wireFrame)
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	else
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	Clear();
	Draw();
	Swap();
}

void Renderer::Clear(){
	glClearColor(0.45f, 0.55f, 1.0f, 1.0f);
	glClearDepth(1.0);
	glClearStencil(GL_STENCIL_CLEAR_VALUE);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
}

void Renderer::Draw(){
	for(auto renderQueue : RenderQueue::renderQueue){
		glBindTexture(GL_TEXTURE_2D, renderQueue.second.texture);
		glUseProgram(renderQueue.second.shaderHandle);
		
		glBindBuffer(GL_ARRAY_BUFFER, renderQueue.second.vbo);
		glBindVertexArray(renderQueue.second.vao);
		glDrawArrays(renderQueue.second.primitive, 0, renderQueue.second.indiceCount);

		glUseProgram(0);
		glBindVertexArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}
}

void Renderer::Swap(){
	wglSwapLayerBuffers(hdc, WGL_SWAP_MAIN_PLANE);
}