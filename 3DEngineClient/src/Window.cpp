#include "Window.h"
#include <iostream>

LRESULT CALLBACK WindowProc(HWND window, UINT msg, WPARAM wparam, LPARAM lparam);

RECT Window::wr = { 0,0,800,600};
HWND Window::window = nullptr;
bool Window::maximized = false;

Window::Window(int windowWidth, int windowHeight, const char* apptitle)
{
	wr.right = windowWidth;
	wr.bottom = windowHeight;

	WNDCLASSEX wc;
	ZeroMemory(&wc, sizeof(WNDCLASSEX));
	wc.cbSize = sizeof(WNDCLASSEX);
	wc.style = CS_OWNDC;
	wc.lpfnWndProc = WindowProc;
	wc.hInstance = GetModuleHandle(NULL);
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);
	wc.hbrBackground = CreateSolidBrush(RGB(0, 0, 0));
	wc.lpszClassName = "WindowClass1";

	RegisterClassEx(&wc);

	AdjustWindowRect(&wr, WS_OVERLAPPEDWINDOW, FALSE);

	window = CreateWindowEx(
		NULL,
		wc.lpszClassName,
		apptitle,
		WS_OVERLAPPEDWINDOW,
		0,
		0,
		windowWidth,
		windowHeight,
		NULL,
		NULL,
		GetModuleHandle(NULL),
		NULL);

	hdc = GetDC(window);
	this->window = window;

	PIXELFORMATDESCRIPTOR pfd =	{
		sizeof(PIXELFORMATDESCRIPTOR),
		1,
		PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER,    //Flags
		PFD_TYPE_RGBA,            //The kind of framebuffer. RGBA or palette.
		32,                        //Colordepth of the framebuffer.
		0, 0, 0, 0, 0, 0,
		0,
		0,
		0,
		0, 0, 0, 0,
		24,                        //Number of bits for the depthbuffer
		8,                        //Number of bits for the stencilbuffer
		0,                        //Number of Aux buffers in the framebuffer.
		PFD_MAIN_PLANE,
		0,
		0, 0, 0
	};

	int pixelFormat = ChoosePixelFormat(hdc, &pfd);

	SetPixelFormat(hdc, pixelFormat, &pfd);

	HGLRC tempRC = wglCreateContext(hdc);
	wglMakeCurrent(hdc, tempRC);

	if (glewInit() != GLEW_OK) {
	}

	DestroyWindow(window);
	wglDeleteContext(tempRC);

	window = CreateWindowEx(
		NULL,
		wc.lpszClassName,
		apptitle,
		WS_POPUP,
		0,
		0,
		windowWidth,
		windowHeight,
		NULL,
		NULL,
		GetModuleHandle(NULL),
		NULL);
	
	LONG lStyle = GetWindowLong(window, GWL_STYLE);
	lStyle &= ~(WS_CAPTION | WS_THICKFRAME | WS_MINIMIZE | WS_MAXIMIZE | WS_SYSMENU);
	SetWindowLong(window, GWL_STYLE, lStyle);
	hdc = GetDC(window);

	UINT numpfs;

	int attributes[] = {
		WGL_DRAW_TO_WINDOW_ARB, GL_TRUE,
		WGL_SUPPORT_OPENGL_ARB, GL_TRUE,
		WGL_DOUBLE_BUFFER_ARB, GL_TRUE,
		WGL_PIXEL_TYPE_ARB, WGL_TYPE_RGBA_ARB,
		WGL_COLOR_BITS_ARB, 32,
		WGL_DEPTH_BITS_ARB, 24,
		WGL_STENCIL_BITS_ARB, 8,
		WGL_SAMPLE_BUFFERS_ARB, 1, //Number of buffers (must be 1 at time of writing)
		WGL_SAMPLES_ARB, 8,        //Number of samples
		0
	};

	if (!wglChoosePixelFormatARB(hdc, attributes, NULL, 1, &pixelFormat, &numpfs))
	{
		std::cout<< "Failed to get a valid Opengl Pixel format:  " << GetLastError() << '\n';
	}

	SetPixelFormat(hdc, pixelFormat, &pfd);

	renderingContext = wglCreateContext(hdc);

	wglMakeCurrent(hdc, NULL);

	wglMakeCurrent(hdc, renderingContext);

	ShowWindow(window, SW_SHOW);

	ShowCursor(false);
}

void Window::HandleEvents()
{
	InputHandler::ResetInputs();
	MSG msg;
	while (PeekMessage(&msg, window, 0, 0, PM_REMOVE))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	if (InputHandler::GetKeyDown(VK_RETURN))
	{
		Window::maximized = !Window::maximized;
		if (!Window::maximized)
		{
			ShowWindow(Window::window, SW_NORMAL);

		}
		if (Window::maximized)
		{
			ShowWindow(Window::window, SW_MAXIMIZE);
		}
	}

}

Window::~Window()
{
	DestroyWindow(window);
}

void Window::SetCursorPosition(int x, int y)
{
	POINT pos;
	pos.x = x;
	pos.y = y;
	ClientToScreen(window, &pos);
	SetCursorPos(pos.x, pos.y);
}


LRESULT CALLBACK WindowProc(HWND window, UINT msg, WPARAM wparam, LPARAM lparam)
{
	switch (msg)
	{
	case WM_DESTROY:
	{
		Engine::isRunning = false;
		PostQuitMessage(0);
	} break;

	case WM_SIZE:
	{
		GetWindowRect(window, &Window::wr);
		AdjustWindowRect(&Window::wr, 0, FALSE);

		glViewport(0,0, Window::wr.right, Window::wr.bottom);
		glScissor(0, 0, Window::wr.right, Window::wr.bottom);

	}break;
	case WM_KEYDOWN:
	{
		InputHandler::HandleInput(INPUT_KEYDOWN, wparam);
	}break;
	case WM_KEYUP:
	{
		InputHandler::HandleInput(INPUT_KEYUP, wparam);
	}break;
	case WM_SYSKEYDOWN:
	{
		InputHandler::HandleInput(INPUT_KEYDOWN, wparam);
	}break;
	case WM_SYSKEYUP:
	{
		InputHandler::HandleInput(INPUT_KEYUP, wparam);
	}break;
	case WM_MOUSEMOVE:
	{
		InputHandler::SetMouse(glm::vec2(GET_X_LPARAM(lparam), GET_Y_LPARAM(lparam)), wparam);
	}break;
	case WM_MOUSEHWHEEL:
	{
		InputHandler::HandleInput(INPUT_MOUSEWHEEL, wparam);
	}break;
	default:
	{
		return DefWindowProc(window, msg, wparam, lparam);
	}
	}
	return 0;
}
