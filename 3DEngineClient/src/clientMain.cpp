#include "Engine.h"
#define WIN32_LEAN_AND_MEAN 
#define VC_EXTRALEAN 
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"
extern "C" {
	_declspec(dllexport) DWORD NvOptimusEnablement = 0x00000001;
}

int main()
{
	Engine engine;
	while (engine.Update()) {
	}
	return 0;
}

