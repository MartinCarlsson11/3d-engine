#include "Light.h"

#include "glm\gtx\transform.hpp"
Light::Light(glm::vec3 color, glm::vec3 position)
{
	this->position = position;
	this->color = color;
	constant = 1.0f;
	linear = 0.007f;
	exponent = 0.0002f;
}

void Light::SetColor(glm::vec3 color)
{
	this->color = color;
}

void Light::SetPosition(glm::vec3 position)
{
	this->position = position;
}

void Light::Move(glm::vec3 offsetPos)
{
	position += offsetPos;
}

void Light::SetSize(int size)
{

}

float Light::GetConstant() const
{
	return constant;
}

float Light::GetExponent() const
{
	return exponent;
}

float Light::GetLinear() const
{
	return linear;
}

glm::vec3 Light::GetColor() const
{
	return color;
}

glm::vec3 Light::GetPosition() const
{
	return position;
}
