#include "RenderSystem.h"
#include "Window.h"
#include "glm\gtx\euler_angles.hpp"
#include "glm\gtx\transform.hpp"
#include "glm\gtc\matrix_transform.hpp"

RenderSystem::RenderSystem()
{
	initMask = 0;
	lightInit = 0;
	camera = std::make_shared<OrbitCamera>();
	observerCamera = std::make_shared<FPSCamera>();
	resourceManager = std::unique_ptr<ResourceManager>(new ResourceManager());
	drawColliders = false;
	resourceManager->AddLight(glm::vec3(256.0f, 20.0f, 256.0f), glm::vec3(1.0f, 1.0f, 1.0f));
}

void RenderSystem::Update(std::vector<std::shared_ptr<Entity>> entities, int playerID)
{
	if (InputHandler::GetKeyDown(KEY_9))
	{
		drawColliders = !drawColliders;
	}
	if (playerID >= 0)
	{
		if (entities[playerID])
		{
			glm::vec3 cameraPosition = glm::vec3(entities[playerID]->pX, entities[playerID]->pY, entities[playerID]->pZ);
			glm::vec3 direction = glm::vec3(entities[playerID]->dX, 1.0f, entities[playerID]->dZ);

			float cameraOffsetUp = 20.0f;
			float cameraOffsetXZ = 15.0f;
			glm::vec3 cameraBehindPlayerOffset = glm::vec3(-direction.x *cameraOffsetXZ, direction.y * cameraOffsetUp, -direction.z * cameraOffsetXZ);
			cameraPosition += cameraBehindPlayerOffset;

			camera->SetPosition(glm::vec3(cameraPosition));

			glm::vec3 cameraLookAtPos = glm::vec3(entities[playerID]->pX + 15.0f * entities[playerID]->dX, entities[playerID]->pY + 12.5f, entities[playerID]->pZ + 15.0f * entities[playerID]->dZ);
			camera->SetLookAt(cameraLookAtPos);
			camera->SetRadius(10.0f);
		}
	}
	else
	{
		//observerCamera set position
		//update rotation
		//move around
	}


	std::vector<glm::mat4> models;
	std::vector<int> id;
	std::vector<glm::vec3> lightPositions;
	std::vector<glm::vec3> lightColors;

	std::vector<int> lightIds;

	for (int i = 0; i <MAXENTITIES; i++)
	{
		if (entities[i] != nullptr)
		{
			if ((entities[i]->componentIndex >> Components::Renderable) & 1)
			{
				if (!((initMask >> i) & 1))
				{

					RenderInformation renderInfo = resourceManager->Load(entities[i]->meshID, i);
					initMask |= (1 << i);
					entities[i]->renderId = RenderQueue::AddDrawCall(renderInfo);
				}
	
				glm::mat4 model;
				model = glm::translate(model, glm::vec3(entities[i]->pX, entities[i]->pY, entities[i]->pZ)) * glm::scale(model, glm::vec3(entities[i]->sX, entities[i]->sY, entities[i]->sZ));

				glm::mat4 rotation = glm::rotate(entities[i]->rX, glm::vec3(0.0f, 1.0f, 0.0f));
				model *= rotation;

				models.push_back(model);
				id.push_back(i);
			}
			if ((entities[i]->componentIndex >> Components::Light) & 1)
			{
				if (!((lightInit >> i) & 1))
				{
					entities[i]->lightId = resourceManager->AddLight(glm::vec3(entities[i]->pX, entities[i]->pY, entities[i]->pZ), glm::vec3(156.f / 255.f, 42.f / 255.f, 0.f / 255.f));
					lightInit |= (1 << i);
				}

				lightPositions.push_back(glm::vec3(entities[i]->pX, entities[i]->pY, entities[i]->pZ ));
				
				lightIds.push_back(entities[i]->lightId);
			}

		}
	}

	resourceManager->UpdateShaders(id, models, camera->GetViewMatrix(), camera->GetPosition(), 
		glm::perspective(camera->GetFOV(), (float)Window::wr.right / (float)Window::wr.bottom, 1.0f, 600.0f), 
		lightIds, lightPositions);
	id.clear();
	models.clear();
}

void RenderSystem::RemoveEntity(unsigned int id, int lightId, unsigned int renderId)
{
	if (lightId != -1)
	{
		resourceManager->RemoveLight(lightId);
		lightInit &= ~(1 << id);
	}
	if (renderId != -1)
	{
		RenderQueue::RemoveDrawCall(renderId);
		initMask &= ~(1 << id);
	}
}
