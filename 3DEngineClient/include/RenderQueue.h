#pragma once
#include "GL\glew.h"
#include "GL.h"
#include <map>
struct RenderInformation
{
	GLuint vao, vbo, shaderHandle;
	GLenum primitive = GL_TRIANGLES;
	GLsizei indiceCount;
	GLuint texture;
};

class RenderQueue
{
public:
	static unsigned int AddDrawCall(RenderInformation information);
	static void RemoveDrawCall(int index);
	static std::map<unsigned int, RenderInformation> renderQueue;
	static unsigned int renderCallCount;
};

