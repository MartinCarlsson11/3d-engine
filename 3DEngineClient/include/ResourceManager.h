#pragma once
#include "Resources.h"
#include <string>
#include <map>
#include <memory>
#include <vector>

#include "Texture2D.h"
#include "Animation.h"
#include "Mesh.h"
#include "Shader.h"
#include "RenderQueue.h"
#include "HeightMap.h"
#include "LightSystem.h"
#include "glm\glm.hpp"
#include "glm\gtx\quaternion.hpp"

class ResourceManager
{
public:
	ResourceManager();
	~ResourceManager();

	RenderInformation Load(unsigned int id, unsigned int entityID);

	void UpdateShaders(std::vector<int> id, std::vector<glm::mat4> models, glm::mat4 view, glm::vec3 viewPos, glm::mat4 projection, std::vector<int> lightIds, std::vector<glm::vec3> lightPositions);
	std::shared_ptr<Mesh> LoadMesh(std::string fileName);
	std::shared_ptr<Shader> LoadShader(std::string vertShader, std::string fragShader);
	std::shared_ptr<Animation> LoadAnimation(std::string fileName);
	int AddLight(glm::vec3 position, glm::vec3 color);
	void RemoveLight(unsigned int id);
	GLuint LoadTexture(std::string fileName);

private:
	FbxManager* fbxManager;
	std::map<std::string, std::shared_ptr<Animation>> animations;
	std::map<std::string, std::shared_ptr<Mesh>> meshes;
	std::map<std::string, std::shared_ptr<Texture2D>> textures;
	std::map<std::pair<std::string, std::string>, std::string> shaderSource;


	std::vector<std::shared_ptr<Shader>> shaders;
	std::unique_ptr<LightSystem> lightSystem;

	const glm::vec3 materialAmbience = glm::vec3(0.2f, 0.2f, 0.2f);
	const glm::vec3 materialSpecular = glm::vec3(0.5f, 0.5f, 0.5f);
};