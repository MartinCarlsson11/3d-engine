#pragma once
#include <vector>
#include <memory>

#include "gl\glew.h"
#include "GL.h"
#include "glm\glm.hpp"
#include "Light.h"

#define MAXLIGHTS 12

struct LightSystem
{
	void Init();
	void Update(std::vector<int> ids, std::vector<glm::vec3> positions, glm::vec3 viewPos);
	int AddLight(glm::vec3 position, glm::vec3 color);
	void RemoveLight(unsigned int id);
	GLuint GetLightUniformBlockBindPoint();

private:
	GLuint lightBlockBindPoint, lightBlockHandle;
	std::vector<std::shared_ptr<Light>> lights;
	unsigned int lightMask;
	int lightCount;

	const float lightConstant = 1.0f;
	const float lightLinear = 0.022f;
	const float lightExponent = 0.0019f;
};