#pragma once
#include <memory>
#include <vector>
#include "NetworkManager.h"
#include "Camera.h"

#include "Entity.h"
#include "MoveSystem.h"
#include "PlayerSystem.h"
#include "RenderSystem.h"
#include <fbxsdk.h>
#include "PhysicsSystem.h"
#include "CollisionSystem.h"

class TestState : public IState {
public:
	TestState();
	bool Update()override;
	void Input(std::vector<char> inputs, Address sender);
	IState* NextState() override;
private:
	bool accepted;
	bool gameStateReceived;

	std::unique_ptr<NetworkManager> networkManager;

	std::unique_ptr<MoveSystem> moveSystem;
	std::unique_ptr<RenderSystem> renderSystem;
	std::unique_ptr<PlayerSystem> playerSystem;
	std::unique_ptr<PhysicsSystem> physicsSystem;
	std::unique_ptr<CollisionSystem> collisionSystem;

	std::vector<std::shared_ptr<Entity>> entities;

	int playerID;
	unsigned int frameCounter;
	unsigned int clientKeyState;

	void HandleInputs();
};

