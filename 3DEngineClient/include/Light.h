#pragma once
#include "Mesh.h"
#include "Shader.h"
class Light
{
public:
	Light(glm::vec3 color, glm::vec3 position);
	void SetColor(glm::vec3 color);
	void SetPosition(glm::vec3 position);
	void Move(glm::vec3 offsetPos);
	void SetSize(int size);

	float GetConstant()const;
	float GetExponent()const;
	float GetLinear()const;
	glm::vec3 GetColor()const;
	glm::vec3 GetPosition()const;
private:
	glm::vec3 position;
	glm::vec3 color;
	float constant;
	float exponent;
	float linear;
};

