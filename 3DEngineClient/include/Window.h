#pragma once
#include "WindowsHeaderIsBullshit.h"
#include "Window.h"
#include "Engine.h"
#include "InputHandler.h"

class Window
{
public:
	Window(int windowWidth, int windowHeight, const char* apptitle);
	~Window();

	static void SetCursorPosition(int x, int y);

	void HandleEvents();
	static RECT wr;	
	static HWND window;
	static bool maximized;
private:
	int* pixelFormat;
	HDC hdc;
	HGLRC renderingContext;
};

