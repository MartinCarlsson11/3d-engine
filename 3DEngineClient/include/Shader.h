#pragma once
#include "GL\glew.h"
#include <gl\GL.h>
#include "glm\glm.hpp"
#include <map>
#include <vector>
class Shader
{
public:
	enum ShaderType
	{
		VERTEX,
		FRAGMENT,
		PROGRAM
	};

	Shader();
	~Shader();
	void LoadShaders(const char* vsFilename, const char* fsFilename);
	void Use();

	GLint GetUniformLocation(const GLchar* name);
	GLuint GetHandle()const;

	void SetUniform(const GLchar* name, const glm::vec2& v);
	void SetUniform(const GLchar* name, const glm::vec3& v);
	void SetUniform(const GLchar* name, const glm::vec4& v);
	void SetUniform(const GLchar* name, std::vector<float>& m, unsigned int count);
	void SetUniform(const GLchar* name, const glm::mat4& m);
	void SetUniform(const GLchar* name, const GLfloat f);
	void SetUniform(const GLchar* name, const GLint i);
	void SetUniformSampler(const GLchar* name, const GLint slot);
	void SetUniformBlock(const GLchar* name, const GLint blockIndex);
private:
	std::string FileToString(const std::string& filename);
	void CheckCompileErrors(GLuint shader, ShaderType type);
	std::map<std::string, GLint> mUniformLocations;
	GLuint handle;
};

