#pragma once
#include <memory>
#include "Entity.h"
#include "RenderQueue.h"
#include "ResourceManager.h"
#include "Camera.h"
#include "glm\glm.hpp"

struct RenderSystem
{
	RenderSystem();

	void Update(std::vector<std::shared_ptr<Entity>> entities, int playerID);
	std::unique_ptr<ResourceManager> resourceManager;
	std::shared_ptr<OrbitCamera> camera;
	std::shared_ptr<FPSCamera> observerCamera;
	void RemoveEntity(unsigned int id, int lightId, unsigned int renderId);

	unsigned int initMask;
	unsigned int lightInit;
	bool drawColliders;
};