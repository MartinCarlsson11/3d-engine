#pragma once
#include "glm\glm.hpp"
#include "glm\gtc\constants.hpp"

class Camera
{
public:
	glm::mat4 GetViewMatrix()const;

	const glm::vec3& GetLook()const;
	const glm::vec3& GetRight()const;
	const glm::vec3& GetUp()const;

	virtual void SetPosition(const glm::vec3& position){}
	glm::vec3 GetPosition()const;

	float GetFOV()const { return fov; }
protected:

	Camera();
	float fov;
	float yaw;
	float pitch;
	glm::vec3 position;
	glm::vec3 targetPos;
	glm::vec3 up;
	glm::vec3 look;
	glm::vec3 right;
	glm::vec3 worldUp;

};

class FPSCamera : public Camera
{
public:
	FPSCamera(glm::vec3 position = glm::vec3(0.0f, 0.0f, 0.0f), float yaw = 3.14159265359f, float pitch = 0.0f);
	void SetPosition(const glm::vec3& position) override;
	void SetRotation(float yaw);
	void Rotate(float yaw, float pitch);
	void Move(const glm::vec3& offsetpos);

private:
	void UpdateCameraVectors();
};

class OrbitCamera : public Camera
{
public:
	OrbitCamera();	
	void SetPosition(const glm::vec3& position)override;
	void SetLookAt(const glm::vec3& target);
	void SetRadius(float radius);
private:

	void UpdateCameraVectors();

	float radius;
};