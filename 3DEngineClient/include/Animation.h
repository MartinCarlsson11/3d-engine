#pragma once
#include <vector>
#include "Mesh.h"
#include "glm\glm.hpp"

struct KeyFrame
{
	std::vector<glm::mat4> matrixes;
};

class Animation
{
public:
	Animation();
	~Animation();
	void LoadAnimation(std::vector<MeshBone> bindPose, std::vector<FbxNode*> skeletonNodes);
	void Update(float deltaTime);

private:
	//Each animation contains a vector of key frame times with corrensponding keyframe
	std::vector<std::vector<std::pair<float, KeyFrame>>> animation;
};