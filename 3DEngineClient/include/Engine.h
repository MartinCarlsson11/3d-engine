#pragma once
#include "Renderer.h"
#include "Window.h"
#include "StateMachine.h"
#include <chrono>
#include <memory>

class Renderer;
class Window;
 
class Engine
{
public:
	Engine();
	~Engine();
	bool Update();
	static bool isRunning;

private:
	std::unique_ptr<Renderer>renderer;
	std::unique_ptr<Window> window;
	std::unique_ptr<StateMachine> stateMachine;
	
	bool TimeStep();
	std::chrono::time_point<std::chrono::high_resolution_clock> lastTime;
	std::chrono::time_point<std::chrono::high_resolution_clock> currentTime;
	std::chrono::nanoseconds deltaTime;
	double targetTime;
	double timeStepAccumulator;
	double frameTimeAccumulator;
	int fps;
};

