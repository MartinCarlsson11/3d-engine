#pragma once
#include <string>
#include <GL\glew.h>
#include <GL\GL.h>
#include "glm\glm.hpp"


#include <vector>
class Texture2D
{
public:
	Texture2D();
	~Texture2D();
	void LoadTexture(const char* fileName);
	void LoadTexture(char* pixels, int width, int height);
	GLuint GetTextureHandle()const;
	void Bind();
private:
	GLuint textureHandle;

};

