#pragma once

#include "WindowsHeaderIsBullshit.h"
#include "gl\glew.h"
#include "GL\wglew.h"
#include "GL.h"
#include "GLU.h"

#pragma comment(lib, "opengl32.lib")

class Renderer
{
public:
	Renderer(HWND window);
	void Update();
private:
	void Clear();
	void Draw();
	void Swap();
	HDC hdc;
	bool wireFrame;
};

