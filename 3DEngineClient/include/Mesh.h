#pragma once
#include "glm\glm.hpp"
#include "GL\glew.h"
#include <vector>
#include <string>
#include <iostream>
#include <fbxsdk.h>
#include "Vertex.h"
struct MeshBone
{
	FbxAMatrix globalPoseMatrix;
};

class Mesh
{
public:
	Mesh();
	~Mesh();
	void LoadMesh(FbxMesh* mesh);
	void LoadMesh(const std::string& filename);
	GLuint GetVertexArray() const;
	int GetVertexCount()const;
	int GetBuffer()const;
	void LoadMeshFromVertices(std::vector<Vertex> vertices);

	std::vector<MeshBone> GetBindPose()const;

private:
	std::vector<Vertex> vertices;
	std::vector<MeshBone> meshBones;
	GLuint vbo, vao;
	void InitBuffers();
	void DeleteBuffers();
	std::vector<std::string> Split(std::string input, std::string delimiter);
};
