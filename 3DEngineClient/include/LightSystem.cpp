#include "LightSystem.h"

void LightSystem::Init()
{
	lightBlockBindPoint = 1;
	lightBlockHandle = 0;
	lightCount = 0;
	lightMask = 0;
	glGenBuffers(1, &lightBlockHandle);
	glBindBuffer(GL_UNIFORM_BUFFER, lightBlockHandle);
	glBufferData(GL_UNIFORM_BUFFER, (MAXLIGHTS * 80) + 20, NULL, GL_DYNAMIC_DRAW);
	glBindBufferBase(GL_UNIFORM_BUFFER, lightBlockBindPoint, lightBlockHandle);

	lights.resize(MAXLIGHTS);
}

void LightSystem::Update(std::vector<int> ids, std::vector<glm::vec3> positions, glm::vec3 viewPos)
{
	glBindBuffer(GL_UNIFORM_BUFFER, lightBlockHandle);
	glm::vec4 view = glm::vec4(viewPos, 1.0f);
	glBufferSubData(GL_UNIFORM_BUFFER, MAXLIGHTS * 80 + 4, sizeof(glm::vec4), &view);
	glBufferSubData(GL_UNIFORM_BUFFER, (MAXLIGHTS * 80), sizeof(GLint), &lightCount);
	for (int i = 0; i < positions.size(); i++)
	{
		if (ids[i] != MAXLIGHTS)
		{
			if (lights[ids[i]])
			{
				lights[ids[i]]->SetPosition(positions[i]);

				GLint offset = ((GLint)(ids[i] * 80));
				glm::vec4 position = glm::vec4(positions[i], 1.0f);
				
				glBufferSubData(GL_UNIFORM_BUFFER, offset, sizeof(glm::vec4), &position);
			}
		}
	}
	glBindBuffer(GL_UNIFORM_BUFFER, 0);
}

int LightSystem::AddLight(glm::vec3 position, glm::vec3 color)
{
	for (int i = 0; i < MAXLIGHTS; i++)
	{
		if (!((lightMask >> i) & 1))
		{
			lights[i] = std::shared_ptr<Light>(new Light(color, position));
			lightCount++;
			lightMask |= (1 << i);

			GLint offset = (GLint)(i*80);

			glm::vec4 ambient = glm::vec4(0.2, 0.2, 0.2, 1.0f);
			glm::vec4 specular = glm::vec4(lights[i]->GetColor() / 4.0f, 1.0f);
			glm::vec4 attenuation = glm::vec4(lights[i]->GetConstant(), lights[i]->GetLinear(), lights[i]->GetExponent(), 1.0f);
			glm::vec4 position = glm::vec4(lights[i]->GetPosition(), 1.0f);
			glm::vec4 color = glm::vec4(lights[i]->GetColor(), 1.0f);

			glBindBuffer(GL_UNIFORM_BUFFER, lightBlockHandle);

			glBufferSubData(GL_UNIFORM_BUFFER, (MAXLIGHTS * 80), sizeof(GLint), &lightCount);
			glBufferSubData(GL_UNIFORM_BUFFER, offset, sizeof(glm::vec4), &position);
			glBufferSubData(GL_UNIFORM_BUFFER, offset + 16, sizeof(glm::vec4), &ambient);
			glBufferSubData(GL_UNIFORM_BUFFER, offset + 32, sizeof(glm::vec4), &color);
			glBufferSubData(GL_UNIFORM_BUFFER, offset + 48, sizeof(glm::vec4), &specular);
			glBufferSubData(GL_UNIFORM_BUFFER, offset + 64, sizeof(glm::vec4), &attenuation);
			glBindBuffer(GL_UNIFORM_BUFFER, 0);
			return i;
		}
	}
	return MAXLIGHTS;
}

void LightSystem::RemoveLight(unsigned int id)
{
	if (id != MAXLIGHTS)
	{
		if (lights[id])
		{
			lightMask &= ~(1 << id);
			lights[id].reset();
			GLint offset = (GLint)(id * 80);

			glm::vec4 ambient = glm::vec4(0.0f, 0.0f, 0.0f, 0.0f);
			glm::vec4 specular = glm::vec4(0.0f, 0.0f, 0.0f, 0.0f);
			glm::vec4 attenuation = glm::vec4(0.0f, 0.0f, 0.0f, 0.0f);
			glm::vec4 position = glm::vec4(0.0f, 0.0f, 0.0f, 0.0f);
			glm::vec4 color = glm::vec4(0.0f, 0.0f, 0.0f, 0.0f);

			glBindBuffer(GL_UNIFORM_BUFFER, lightBlockHandle);
			glBufferSubData(GL_UNIFORM_BUFFER, offset, sizeof(glm::vec4), &position);
			glBufferSubData(GL_UNIFORM_BUFFER, offset + 16, sizeof(glm::vec4), &ambient);
			glBufferSubData(GL_UNIFORM_BUFFER, offset + 32, sizeof(glm::vec4), &color);
			glBufferSubData(GL_UNIFORM_BUFFER, offset + 48, sizeof(glm::vec4), &specular);
			glBufferSubData(GL_UNIFORM_BUFFER, offset + 64, sizeof(glm::vec4), &attenuation);
			glBindBuffer(GL_UNIFORM_BUFFER, 0);
		}
	}
}

GLuint LightSystem::GetLightUniformBlockBindPoint()
{
	return lightBlockBindPoint;
}
