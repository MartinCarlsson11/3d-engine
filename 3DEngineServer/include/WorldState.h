#pragma once
#include "IState.h"
#include "NetworkManager.h"
#include "Entity.h"
#include <vector>
#include <memory>
#include "PlayerSystem.h"
#include "PhysicsSystem.h"
#include "CollisionSystem.h"

struct Keystate
{
	unsigned int frame;
	unsigned int keyState;
	unsigned int playerID;
};

class WorldState : public IState{
public:
	WorldState();
	~WorldState();
	bool Update()override;
	void Input(std::vector<char> input, Address sender) override;
	IState* NextState() override;
private:
	std::unique_ptr<NetworkManager> networkManager;

	std::vector<std::shared_ptr<Entity>> entities;

	std::unique_ptr<PhysicsSystem> physicsSystem;
	std::unique_ptr<PlayerSystem> playerSystem;
	std::unique_ptr<CollisionSystem> collisionSystem;

	std::vector<Entity> requestCreate;
	std::vector<int> requestRemove;
	std::vector<Address> clients;

	void SendGameState();

	unsigned int AddEntity();
	void RemoveEntity(int id);

	unsigned int entityMask;
	unsigned int entityCount;
	unsigned int frameCounter;
};

