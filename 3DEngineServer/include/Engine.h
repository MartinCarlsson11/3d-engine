#pragma once

#include "StateMachine.h"
#include <chrono>

class Engine
{
public:
	Engine();
	~Engine();
	bool Update();
	static bool isRunning;
private:
	StateMachine* stateMachine;	

	bool TimeStep();
	std::chrono::time_point<std::chrono::high_resolution_clock> lastTime;
	std::chrono::time_point<std::chrono::high_resolution_clock> currentTime;
	std::chrono::nanoseconds deltaTime;
	double targetTime;
	double timeStepAccumulator;
	double frameTimeAccumulator;
	int fps;
};

