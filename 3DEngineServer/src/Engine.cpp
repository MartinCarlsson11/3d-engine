#include "Engine.h"
#include <iostream>
#include "WorldState.h"
bool Engine::isRunning = false;

Engine::Engine()
	:targetTime(1.0 / 60.0),
	timeStepAccumulator(0.0),
	frameTimeAccumulator(0.0),
	fps(0),
	lastTime(std::chrono::high_resolution_clock::now()){
	stateMachine = new StateMachine(new WorldState());
	isRunning = true;
	stateMachine->Start();
}	

Engine::~Engine(){
	delete stateMachine;
	stateMachine = nullptr;
}

bool Engine::Update(){
	if (TimeStep()) 
		if (!stateMachine->Update()) 
			isRunning = false;
	return isRunning;
}

bool Engine::TimeStep(){
	currentTime = std::chrono::high_resolution_clock::now();
	auto deltaTime = (currentTime - lastTime) / 1000000000.0;

	timeStepAccumulator += deltaTime.count(); 
	frameTimeAccumulator += deltaTime.count();

	if (frameTimeAccumulator > 1.0){	
		std::cout << "FPS: " << fps << std::endl;
		frameTimeAccumulator = 0;
		fps = 0;
	}

	lastTime = currentTime;

	if (timeStepAccumulator > targetTime){
		timeStepAccumulator = 0;
		fps++;
		return true;
	}
	else return false;
}