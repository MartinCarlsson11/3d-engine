#include "WorldState.h"
#include "Packet.h"
#include "Serialization.h"
#include "Resources.h"

WorldState::WorldState()
	:entityMask(0),
	frameCounter(0x00),
	entityCount(0)	
{
	playerSystem = std::make_unique<PlayerSystem>();
	physicsSystem = std::make_unique<PhysicsSystem>();
	collisionSystem = std::make_unique<CollisionSystem>();

	networkManager = std::unique_ptr<NetworkManager>(new NetworkManager(_SERVER));
	entities.resize(MAXENTITIES);
	unsigned int entityID = AddEntity();
	if (entityID != MAXENTITIES)
	{
		entities[entityID]->componentIndex |= Components::RENDERABLE;
		entities[entityID]->componentIndex |= Components::HEIGHTMAP;
		entities[entityID]->pX = 0.0f;
		entities[entityID]->pY = 0.0f;
		entities[entityID]->pZ = 0.0f;
		entities[entityID]->sX = 1.0f;
		entities[entityID]->sY = 1.0f;
		entities[entityID]->sZ = 1.0f;
		entities[entityID]->meshID = Meshes::HEIGHTMAPFLOOR;
	}

	entityID = AddEntity();
	if (entityID != MAXENTITIES)
	{
		entities[entityID]->componentIndex |= Components::RENDERABLE;
		entities[entityID]->pX = 0.0f;
		entities[entityID]->pY = 100.0f;
		entities[entityID]->pZ = 0.0f;
		entities[entityID]->sX = 1.0f;
		entities[entityID]->sY = -1.0f;
		entities[entityID]->sZ = 1.0f;
		entities[entityID]->meshID = Meshes::HEIGHTMAPROOF;
	}
}


WorldState::~WorldState(){

}

bool WorldState::Update(){
	networkManager->Update(this);

	//sort entities

	playerSystem->Update(entities, requestCreate);
	collisionSystem->Update(entities, requestRemove);

	for (int i = 0; i < requestCreate.size(); i++)
	{
		unsigned int entityID = AddEntity();
		if (entityID != MAXENTITIES)
		{
			entities[entityID]->componentIndex = requestCreate[i].componentIndex;
			entities[entityID]->pX = requestCreate[i].pX;
			entities[entityID]->pY = requestCreate[i].pY;
			entities[entityID]->pZ = requestCreate[i].pZ;
			entities[entityID]->sX = requestCreate[i].sX;
			entities[entityID]->sY = requestCreate[i].sY;
			entities[entityID]->sZ = requestCreate[i].sZ;
			entities[entityID]->dX = requestCreate[i].dX;
			entities[entityID]->dY = requestCreate[i].dY;
			entities[entityID]->dZ = requestCreate[i].dZ;
			entities[entityID]->bcX = requestCreate[i].bcX;
			entities[entityID]->bcY = requestCreate[i].bcY;
			entities[entityID]->bcZ = requestCreate[i].bcZ;
			entities[entityID]->velocity = requestCreate[i].velocity;
			entities[entityID]->meshID = requestCreate[i].meshID;
		}
		requestCreate.erase(requestCreate.begin() + i);
	}
	for (int i = 0; i < requestRemove.size(); i++)
	{
		RemoveEntity(requestRemove[i]);
		requestRemove.erase(requestRemove.begin() + i);
	}
	for (int i = 0; i < entities.size(); i++)
	{
		if (entities[i])
		{
			if ((entities[i]->componentIndex >> Components::Player) & 1)
			{
				if (entities[i]->hitPoints < 0)
				{
					RemoveEntity(i);
					//client can observe, but character is removed
				}
			}
		}
	}

	SendGameState();

	frameCounter++;
	return true;
}

void WorldState::Input(std::vector<char> input, Address sender)
{
	if (input[0] == PacketType::INPUT)
	{		
		//Player ID
		int playerID;
		char PID[] = {input[1], input[2], input[3], input[4]};
		memcpy(&playerID, &PID, sizeof(playerID));

		//Frame when inputs supposedly happened
		unsigned int frameID;
		char FID[] = {input[5], input[6], input[7], input[8]};
		memcpy(&frameID, &FID, sizeof(frameID));
	
		//Get Inputs
		unsigned int keyState;
		char KSD[] = {input[9], input[10], input[11], input[12]};
		memcpy(&keyState, &KSD, sizeof(keyState));

		//entities[frameid][playerid]->keyState = keystate;
		if (playerID >= 0)
		{
			if (entities[playerID])
			{
				entities[playerID]->keyStates = keyState;
			}
		}
	}

	if (input[0] == PacketType::JOIN)
	{
		unsigned int player = AddEntity();
		if (player != MAXENTITIES)
		{
			clients.push_back(sender);
			entities[player]->componentIndex = Components::MOVEABLE | Components::PLAYER | Components::RENDERABLE | Components::COLLIDABLE;
			
			//first one is 250, 0, 400
			//3.14
			//second one is 250, 0, 100
			//6.28
			//fourth is 100, 0, 250
			//1.5709
			//third is 400, 0, 250
			//4.7124

			entities[player]->pX = 100.0f;
			entities[player]->pY = 0.0f;
			entities[player]->pZ = 400.0f;

			entities[player]->sX = 1.0f;
			entities[player]->sY = 1.0f;
			entities[player]->sZ = 1.0f;
			entities[player]->meshID = Meshes::CHARACTER2;

			entities[player]->bcX = 4.0f;
			entities[player]->bcY = 15.0f;
			entities[player]->bcZ = 4.0f;

			entities[player]->bcXOffset = 0.0f;
			entities[player]->bcXOffset = 0.0f;
			entities[player]->bcXOffset = 0.0f;


			std::vector<char> data;
			data.push_back(PacketType::ACCEPT);
			Serialize(player, data);

			Packet packet = Packet(data, sender);
			networkManager->AddPacket(packet);

			std::cout << "Player joined, entity ID: " << player << std::endl;
		}
		else
		{
			std::vector<char> data;
			data.push_back(PacketType::DENY);
 			Packet packet = Packet(data, sender);
			networkManager->AddPacket(packet);

			std::cout << "Player request denied" << std::endl;
		}

	}

	if (input[0] == PacketType::DISCONNECT)
	{
		unsigned int playerID = 0; 
		UnSerialize(playerID, input.data(), 1);
		RemoveEntity(playerID);

		auto it = clients.begin();
		while (it != clients.end())
		{
			if ((*it).GetAddress() == sender.GetAddress() && (*it).GetPort() == sender.GetPort())
			{
				clients.erase(it);
				break;
			}
			++it;
		}
	}
}

IState * WorldState::NextState(){
	return nullptr;
}

void WorldState::SendGameState()
{
	std::vector<char> data;
	data.push_back(PacketType::GAMESTATE);

	Serialize(frameCounter, data);
	Serialize(entityCount, data);
	Serialize(entityMask, data);

	for (auto entities : entities)
	{
		if (entities != nullptr)
		{
			Serialize(entities->componentIndex, data);
			Serialize(entities->pX, data);
			Serialize(entities->pY, data);
			Serialize(entities->pZ, data);

			Serialize(entities->rX, data);
			Serialize(entities->rY, data);
			Serialize(entities->rZ, data);

			Serialize(entities->sX, data);
			Serialize(entities->sY, data);
			Serialize(entities->sZ, data);

			//check components
			if (entities->componentIndex >> Components::Move & 1)
			{
				Serialize(entities->dX, data);
				Serialize(entities->dY, data);
				Serialize(entities->dZ, data);
				Serialize(entities->velocity, data);
			}

			if (entities->componentIndex >> Components::Player & 1)
			{
				Serialize(entities->keyStates, data);
				Serialize(entities->hasFired, data);
			}

			if (entities->componentIndex >> Components::Renderable & 1)
			{
				Serialize(entities->meshID, data);
			}

			if (entities->componentIndex >> Components::Light & 1)
			{
			}
			if (entities->componentIndex >> Components::Collidable & 1)
			{
				Serialize(entities->bcX, data);
				Serialize(entities->bcY, data);
				Serialize(entities->bcZ, data);
				Serialize(entities->bcXOffset, data);
				Serialize(entities->bcYOffset, data);
				Serialize(entities->bcZOffset, data);
			}
			if (entities->componentIndex >> Components::Ability_Fireball & 1)
			{
				Serialize(entities->bounceCount, data);
			}
		}
	}

	for (auto clients : clients)
	{
		Packet packet = Packet(data, clients);
		networkManager->AddPacket(packet);
	}
}

unsigned int WorldState::AddEntity()
{
	for (int i = 0; i < MAXENTITIES; i++)
	{
		if (!((entityMask >> i) & 1))
		{
			entities[i] = std::make_shared<Entity>();
			entityCount++;
			entityMask |= (1 << i);
			return i;
		}
	}
	return MAXENTITIES;
}

void WorldState::RemoveEntity(int id)
{
	if (id >= 0)
	{
		if (entities[id])
		{
			entityMask &= ~(1 << id);
			entityCount--;
			entities[id].reset();
			std::vector<char> data;
			data.push_back(PacketType::REMOVEENTITY);

			Serialize(frameCounter, data);
			Serialize(id, data);

			for (auto clients : clients)
			{
				Packet packet = Packet(data, clients);
				networkManager->AddPacket(packet);
			}
		}
	}
}