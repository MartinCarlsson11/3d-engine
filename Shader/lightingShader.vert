#version 330 core

layout (location = 0) in vec3 pos;  
layout (location = 1) in vec3 normal;
layout (location = 2) in vec2 texCoord;
layout (location = 3) in vec4 weights;
layout (location = 4) in ivec4 joints;

uniform mat4 model;			
uniform mat4 view;			
uniform mat4 projection;

uniform mat4 bonesMatrix[75];

out vec2 TexCoord;
out vec3 Normal;
out vec3 FragPos;

void main()
{
	mat4 animationMatrix  = mat4(1.0);

	vec4 position = animationMatrix * vec4(pos, 1.0f);
	gl_Position = projection * view * model * position;
	TexCoord = texCoord;	
	FragPos = vec3(model * animationMatrix * vec4(pos, 1.0f));
	Normal = mat3(transpose(inverse(model * animationMatrix))) * normal;
}
