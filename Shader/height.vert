#version 330 core

layout (location = 0) in vec3 pos;

out vec4 color;

uniform mat4 model;			
uniform mat4 view;			
uniform mat4 projection;	

void main()
{
	gl_Position = projection * view * model * vec4(pos, 1.0f);
	if(pos.y != 0.0)
	{
		color = vec4(0.35f, 0.45f, 1.0 / pos.y, 1.0f);
	}
	else
	{
		color = vec4(0.05f, pos.y , 0.65f, 1.0f);
	}

}