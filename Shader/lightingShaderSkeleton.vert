#version 330 core

layout (location = 0) in vec3 pos;  
layout (location = 1) in vec3 normal;
layout (location = 2) in vec2 texCoord;
layout (location = 3) in vec4 weights;
layout (location = 4) in vec4 joints;

uniform mat4 model;			
uniform mat4 view;			
uniform mat4 projection;
uniform mat4 bonesMatrix[64];

out vec2 TexCoord;
out vec3 Normal;
out vec3 FragPos;

void main()
{
	mat4 animationMatrix =
	weights[0] * bonesMatrix[int(joints[0])] +
	weights[1] * bonesMatrix[int(joints[1])] +
	weights[2] * bonesMatrix[int(joints[2])] +
	weights[3] * bonesMatrix[int(joints[3])];

	gl_Position = projection * view * model* animationMatrix * vec4(pos, 1.0f);
		
	TexCoord = texCoord;
	FragPos = vec3(model * vec4(pos, 1.0f));
	Normal = mat3(transpose(inverse(model * animationMatrix))) * normal;
}