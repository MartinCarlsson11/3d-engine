#version 330 core

struct Material
{
	vec3 ambient;
	sampler2D diffuseMap;
	vec3 specular;
	float shininess;
};

struct Light
{
	vec4 position;
	vec3 ambient;
	vec3 diffuse;
	vec3 specular;
	vec3 attenuation;
};

in vec2 TexCoord;
in vec3 Normal;
in vec3 FragPos;

layout (location = 0) out vec4 frag_color;
layout (location = 1) out vec4 bright_color;

#define MAXLIGHTS 12

layout (std140) uniform lightData
{
	Light light[MAXLIGHTS];
	int numberOfLights;
	vec3 viewPos;
};

uniform Material material;

vec3 CalculateLight(Light light, vec3 normal, vec3 fragPos, vec3 viewDir);

void main()
{
	vec3 normal = normalize(Normal);
	vec3 viewDirection = normalize(viewPos - FragPos);

	//ambient
	vec3 ambient = light[0].ambient * material.ambient * vec3(texture(material.diffuseMap, TexCoord));
	vec3 outColor = vec3(0.0f);

	for(int i = 0; i < numberOfLights; i++)
	{
		outColor += CalculateLight(light[i], normal, FragPos, viewDirection);
	}

	frag_color = vec4(ambient + outColor, 1.0f);
}


vec3 CalculateLight(Light light, vec3 normal, vec3 fragPos, vec3 viewDir)
{
	if(light.position.w != 0.0f)
	{
		//diffuse
		vec3 lightDirection = normalize(vec3(light.position.x, light.position.y,light.position.z) - fragPos);
		float NDotL = max(dot(normal, lightDirection), 0.0f);
		vec3 diffuse = light.diffuse * vec3(texture(material.diffuseMap, TexCoord)) * NDotL;

		//specular
		vec3 halfDirection = normalize(lightDirection + viewDir);
		float NDotH = max(dot(normal, halfDirection), 0.0f);
		vec3 specular = light.specular * material.specular * pow(NDotH, material.shininess);

		float distance = length(vec3(light.position.x, light.position.y,light.position.z) - FragPos);
		float attenuation = 1.0f / (light.attenuation.x + light.attenuation.y * distance + light.attenuation.z * (distance*distance));

		diffuse *= attenuation;
		specular *= attenuation;

		return(diffuse + specular);
	}
	else
	{
		return vec3(0.0f, 0.0f, 0.0f);
	}

}