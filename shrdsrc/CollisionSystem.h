#pragma once
#include <vector>
#include <map>
#include <memory>
#include "glm\glm.hpp"

#include "Entity.h"
#include "HeightMap.h"
struct Box
{
	std::vector<glm::vec3> vertices;
	//glm::vec3 rotation;
	glm::vec3 position;
	//glm::vec3 scale;
};

struct CollisionSystem
{
	void Update(std::vector<std::shared_ptr<Entity>> entities, std::vector<int> &removeRequest);

	unsigned int heightMapInit;
	std::vector<std::vector<Vertex>> terrainMap;
	const int maxHeightMaps = 4;

	bool CheckGJK(Box& box1, Box& box2);
	glm::vec3 Support(Box& box1, Box& box2, const  glm::vec3 &direction);
	bool ContainsOrigin(glm::vec3& a, glm::vec3& b, glm::vec3& c, glm::vec3& d, glm::vec3& dir, int& simplexCount);
	bool CheckTetrahedron(const glm::vec3& ao, const glm::vec3& ab, const glm::vec3& ac, const glm::vec3& abc,
		glm::vec3& a, glm::vec3& b, glm::vec3& c, glm::vec3& d, glm::vec3 direction, int& simplexCount);
	
	Box GetBox(glm::vec3 size);
	glm::vec3 GetFurthestPointInDir(Box& box, glm::vec3 dir);

};


