#pragma once
#include <string>


namespace Meshes
{
	enum
	{
		STANDARD_BOX,
		CHARACTER1,
		CHARACTER2,
		HEIGHTMAPFLOOR,
		HEIGHTMAPROOF,
		NPC1,
		FIREBALL
	};
}

namespace Assets
{
	const std::string assetPath = "../../../assets/";
	const std::string boxPath = "Crate/crate.obj";
	const std::string character1 = "Character1/character1.fbx";
	const std::string character2 = "Character2/character2.fbx";
	const std::string character3 = "Character3/character3.fbx";
	//const std::string character4 = "Character3/character4.fbx";

	const std::string shaderPath = "../../../shader/";
	const std::string standardVert = "lightingShader.vert";
	const std::string standardFrag = "lightingShader.frag";
	const std::string colorVert = "colorShader.vert";
	const std::string colordFrag = "colorShader.frag";
	const std::string boxTexturePath = "crate/crate.jpg";

	const std::string character1TextureDiffuse = "Character1/diffuse.png";
	const std::string character1TextureSpecular = "Character1/specular.png";
	const std::string character1TextureNormal = "Character1/normal.png";
	const std::string character1Run = "Character1/sword_and_shield_run_1.fbx";

	const std::string character2TextureDiffuse = "Character2/diffuse.png";
	const std::string character2TextureSpecular = "Character2/specular.png";
	const std::string character2TextureGlow = "Character2/glow.png";

	const std::string character3TextureDiffuse = "Character3/diffuse.png";
	const std::string character3TextureSpecular = "Character3/specular.png";
	const std::string character3TextureGlow = "Character3/glow.png";
	const std::string character3TextureNormal = "Character3/normal.png";
	const std::string character3Run = "Character3/Standing_Run_Forward.fbx";

	const std::string npc1 = "npc1/npc1.fbx";
	const std::string npc1TextureDiffuse = "npc1/diffuse.png";
	const std::string npc1TextureSpecular = "npc1/specular.png";
	const std::string npc1TextureNormal = "npc1/normal.png";

	const std::string fireball = "fireball/fireball.fbx";
	const std::string fireballDiffuse = "fireball/fireballTexture.jpg";
	const std::string iceballDiffuse = "fireball/iceBallTexture.jpg";

	const std::string heightMapFloor = "HeightMap/floor.png";
	const std::string heightMapRoof = "HeightMap/roof.png";
	const std::string heightMapTexture = "HeightMap/rock.jpg";

}

//Light values
/*
Range Constant Linear Quadratic
3250, 1.0, 0.0014, 0.000007
600, 1.0, 0.007, 0.0002
325, 1.0, 0.014, 0.0007
200, 1.0, 0.022, 0.0019
160, 1.0, 0.027, 0.0028
100, 1.0, 0.045, 0.0075
65, 1.0, 0.07, 0.017
50, 1.0, 0.09, 0.032
32, 1.0, 0.14, 0.07
20, 1.0, 0.22, 0.20
13, 1.0, 0.35, 0.44
7, 1.0, 0.7, 1.8
*/