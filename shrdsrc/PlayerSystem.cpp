#include "PlayerSystem.h"
#include <cmath>
#include "Resources.h"
const glm::vec3 worldUp = glm::vec3(0.0, 1.0, 0.0);

void PlayerSystem::Update(std::vector<std::shared_ptr<Entity>> entities, std::vector<Entity> &requestCreate)
{
	for (int i = 0; i < entities.size(); i++)
	{
		if (entities[i] != nullptr)
		{
			//temporary fireball update
			if ((entities[i]->componentIndex >> Components::Ability_Fireball))
			{

				entities[i]->pX += entities[i]->dX * entities[i]->velocity;
				entities[i]->pZ += entities[i]->dZ * entities[i]->velocity;
			}

			if ((entities[i]->componentIndex >> Components::Player) & 1)
			{
				if ((entities[i]->componentIndex >> Components::Move) & 1)
				{
					glm::vec3 look;
					look.x = sinf(entities[i]->rX);
					look.y = 0.0f;
					look.z = cosf(entities[i]->rX);
					look = glm::normalize(look);

					entities[i]->dX = look.x;
					entities[i]->dY = look.y;
					entities[i]->dZ = look.z;

					if ((entities[i]->keyStates >> PlayerKeys::PLAYER_KEY_W) & 1)
					{
						entities[i]->pZ += entities[i]->dZ;
						entities[i]->pX += entities[i]->dX;
					}
					if ((entities[i]->keyStates >> PlayerKeys::PLAYER_KEY_S) & 1)
					{
						entities[i]->pZ -= entities[i]->dZ;
						entities[i]->pX -= entities[i]->dX;
					}
					if ((entities[i]->keyStates >> PlayerKeys::PLAYER_KEY_A) & 1)
					{
						entities[i]->rX += glm::radians(2.0f);
						if (entities[i]->rX > 2.0f * 3.14f)
						{
							entities[i]->rX = 0;
						}
						entities[i]->rZ += glm::radians(2.0f);
						if (entities[i]->rZ > 2.0f * 3.14f)
						{
							entities[i]->rZ = 0;
						}
					}
					if ((entities[i]->keyStates >> PlayerKeys::PLAYER_KEY_D) & 1)
					{
						entities[i]->rX -= glm::radians(2.0f);
						entities[i]->rZ -= glm::radians(2.0f);

						if (entities[i]->rX < 0.0f)
						{
							entities[i]->rX = 2.0f * 3.14f;
						}
						if (entities[i]->rZ < 0.0f)
						{
							entities[i]->rZ = 2.0f * 3.14f;
						}
					}
				}
				if (entities[i]->fireBallCooldown > 0)
				{
					entities[i]->fireBallCooldown -= 1.0 / 60.0;
				}
				if (entities[i]->fireBallCooldown <= 0)
				{
					entities[i]->hasFired = false;
				}
				if (!entities[i]->hasFired)
				{
					if ((entities[i]->keyStates >> PlayerKeys::PLAYER_KEY_1) & 1)
					{
						//fireball
						Entity fireball;
						fireball.componentIndex = Components::MOVEABLE | Components::ABILITYFIREBALL | Components::RENDERABLE | Components::COLLIDABLE | Components::LIGHT;
						fireball.sX = 0.05f;
						fireball.sY = 0.05f;
						fireball.sZ = 0.05f;
						fireball.dX = entities[i]->dX;
						fireball.dY = entities[i]->dY;
						fireball.dZ = entities[i]->dZ;
						fireball.pX = entities[i]->pX + entities[i]->dX* 10.0f;
						fireball.pY = entities[i]->pY + 15.0f;
						fireball.pZ = entities[i]->pZ + entities[i]->dZ* 10.0f;
						fireball.bcX = 1.0f;
						fireball.bcY = 4.0f;
						fireball.bcZ = 1.0f;

						fireball.velocity = 1.0f;
						fireball.meshID = Meshes::FIREBALL;
						requestCreate.push_back(fireball);

						entities[i]->hasFired = true;
						entities[i]->fireBallCooldown = 2.0f;
					}
				}
			}
		}
	}
}
