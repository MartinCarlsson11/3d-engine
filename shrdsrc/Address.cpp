#include "Address.h"
#include <WinSock2.h>

Address::Address()
{
}

Address::Address(unsigned char a, unsigned char b, unsigned char c, unsigned char d, unsigned short port)
{
	this->port = htons(port);
	address =
		(a << 24) |
		(b << 16) |
		(c << 8) |
		d;
	address = htonl(address);
}

Address::Address(unsigned int address, unsigned short port)
{
	this->address = address;
	this->port = port;
}

unsigned int Address::GetAddress() const
{
	return address;
}

unsigned short Address::GetPort() const
{
	return port;
}
