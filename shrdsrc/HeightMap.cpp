#include "HeightMap.h"
#include "stb_image.h"

HeightMap::HeightMap(std::string fileName)
{
	int width, height, comp;
	unsigned char* imageData;
	imageData = stbi_load(fileName.data(), &width, &height, &comp, STBI_grey);

	std::vector<float> map;
	for (int arrayPos = 0; arrayPos < width * height; )
	{
		map.push_back((imageData[arrayPos] / 255.0));
		arrayPos++;
	}
	stbi_image_free(imageData);

	unsortedVertices.resize(height);
	for (int resize = 0; resize < height; resize++)
	{
		unsortedVertices[resize].resize(width);
	}

	int columns = 0;
	int rows = 0;
	for (; columns < unsortedVertices.size(); columns++)
	{
		for (; rows < unsortedVertices[columns].size(); rows++)
		{
			Vertex vertex;
			vertex.position.x = rows;
			vertex.position.y = map[(columns * width) + rows] * MAX_HEIGHT;
			vertex.position.z = columns;

			vertex.texCoords.x = (float)rows * 0.01f;//vertex.position.x / width;
			vertex.texCoords.y = (float)columns * 0.01f;//vertex.position.z / height;

			unsortedVertices[columns][rows] = vertex;
		}
		rows = 0;
	}

	for (int i = 0; i < height-2; i++)
	{
		for (int x = 0; x < width-2; x++)
		{
			float v1 = 0.0f;
			float v2 = 0.0f;
			float v3 = 0.0f;
			float v4 = 0.0f;
			if (i + 1 < height -1)
			{
				v1 = unsortedVertices[i + 1][x].position.y;
				if (x + 2 < width -1)
				{
					v2 = unsortedVertices[i + 1][x + 2].position.y;
				}
			}
			if (x + 1 < width - 1)
			{
				v3 = unsortedVertices[i][x + 1].position.y;
			}
			if (i + 2 < height - 1)
			{
				if (x + 1 < width - 1)
				{
					v4 = unsortedVertices[i + 2][x + 1].position.y;
				}
			}

			glm::vec3 normal;
			normal.x = v1 - v2;
			normal.y = 2.0f; 
			normal.z = v4 - v3;
			normal = normalize(normal);

			unsortedVertices[i][x].normal = normal;
		}
	}

	columns = height;
	rows = width;

	for (int i = 0; i < columns - 1; i++)
		for (int quad = 0; quad < rows-1; quad++)
		{
			vertices.emplace_back(unsortedVertices[i][quad]);
			vertices.emplace_back(unsortedVertices[i + 1][quad]);
			vertices.emplace_back(unsortedVertices[i + 1][quad + 1]);

			vertices.emplace_back(unsortedVertices[i][quad]);
			vertices.emplace_back(unsortedVertices[i + 1][quad + 1]);
			vertices.emplace_back(unsortedVertices[i][quad + 1]);
		}
}


HeightMap::~HeightMap()
{
	vertices.clear();
}

std::vector<Vertex>& HeightMap::GetVertices()
{
	return vertices;
}

std::vector<std::vector<Vertex>>& HeightMap::GetUnsortedVertices()
{
	return unsortedVertices;
}
