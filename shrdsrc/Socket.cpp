#include "Socket.h"
#include <WinSock2.h>
#include <WS2tcpip.h>
#include <iostream>

Socket::Socket(unsigned int socketType)
{
	this->socketType = socketType;
}

bool Socket::Open(unsigned short port)
{
	handle = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP); //-V103
	if (handle <= 0)
	{
		std::cerr << "failed to create socket";
		return false;
	}

	if (socketType == _SERVER)
	{
		sockaddr_in address;
		address.sin_family = AF_INET;
		address.sin_addr.S_un.S_addr = INADDR_ANY;
		address.sin_port = htons(port);

		if (bind(handle, (const sockaddr*)&address, sizeof(sockaddr_in)) == SOCKET_ERROR) //-V106
		{
			std::cerr << "failed to bind socket";
			return false;
		}
	}
	
	unsigned long nonBlocking = 1;
	if (ioctlsocket(handle, FIONBIO, &nonBlocking) != 0) //-V106
	{
		std::cerr << "failed to put socket in non-blocking mode";
		return false;
	}
	return true;
}

void Socket::Close()
{
	closesocket(handle);
}	

bool Socket::Send(Address  destination, std::vector<char> data)
{
	sockaddr_in addr;
	addr.sin_family = AF_INET;
	addr.sin_addr.s_addr = destination.GetAddress();
	addr.sin_port = destination.GetPort();
	int sent_bytes = sendto(handle, data.data(), data.size(), 0, (const sockaddr*)&addr, addr.sin_port);
	if (sent_bytes != data.size())
	{
		std::cerr << "failed to send packet, Error code: " << WSAGetLastError();
		return false;
	}
	return true;
}

int Socket::Receive(Address & sender, std::vector<char>& data)
{
	typedef int socklen_t;
	sockaddr_in from;
	socklen_t fromLength = sizeof(from);

	int bytes = recvfrom(handle,
		data.data(),
		data.size(),
		0,
		(sockaddr*)&from,
		&fromLength);

	if (bytes <= 0)
		return 0;
		
	sender = Address((from.sin_addr.s_addr), (from.sin_port));

	return fromLength;
}
