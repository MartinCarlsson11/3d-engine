#pragma once
#include <vector>
#include "Vertex.h"
#include "glm\glm.hpp"

class HeightMap
{
public:
	HeightMap(std::string fileName);
	~HeightMap();
	std::vector<Vertex>& GetVertices();
	std::vector<std::vector<Vertex>>& GetUnsortedVertices();

private:
	std::vector<Vertex> vertices;
	std::vector<std::vector<Vertex>> unsortedVertices;

	const float MAX_HEIGHT = 100.0f;
};