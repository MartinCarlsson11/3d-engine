#pragma once
#include <vector>
template <typename T>
void Serialize(T value, std::vector<char> &data) {
	std::vector<char> temp;
	temp.resize(sizeof(value));
	memcpy(temp.data(), &value, sizeof(value));
	for (int i = 0; i < sizeof(value); i++)
	{
		data.push_back(temp[i]);
	}
	temp.clear();
}

template <typename T>
void UnSerialize(T &value, const char data[], size_t index) {
	char temp[] = { data[index], data[index + 1], data[index + 2], data[index + 3] };
	memcpy(&value, &temp, sizeof(value));
}