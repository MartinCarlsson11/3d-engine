#include "NetworkManager.h"
#include <algorithm>
NetworkManager::NetworkManager(unsigned int socketType){
	WSADATA wsaData;
	WSAStartup(MAKEWORD(2, 2), &wsaData);
	socket = new Socket(socketType);
	if (!socket->Open(SERVER_PORT)){
		std::cerr << "failed to open socket, Error code: " << WSAGetLastError();
	}
}

NetworkManager::~NetworkManager(){
	socket->Close();
	WSACleanup();
}

void NetworkManager::Update(IState* state) {
	Address sender;
	int bytesReceived = 0;
	std::vector<char> receiveData;
	receiveData.resize(512);

	while ((bytesReceived = socket->Receive(sender, receiveData)) > 0){
		state->Input(receiveData, sender);
	}

	for (auto sendPackets : sendPackets) {
		socket->Send(sendPackets.GetAddress(), sendPackets.SendPacket());
	}
	sendPackets.clear();

}

void NetworkManager::AddPacket(Packet& packet){
	sendPackets.push_back(packet);
}