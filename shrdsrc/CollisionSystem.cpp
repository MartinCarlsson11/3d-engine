#include "CollisionSystem.h"
#include "Resources.h"
#include "glm\gtx\euler_angles.hpp"
#include "glm\gtx\transform.hpp"
#include "glm\gtc\matrix_transform.hpp"
#include "glm\vec3.hpp"
#include <iostream>

void CollisionSystem::Update(std::vector<std::shared_ptr<Entity>> entities, std::vector<int> &removeRequest)
{
	for (int i = 0; i < entities.size(); i++)
	{
		if (entities[i])
		{
			if ((entities[i]->componentIndex >> Components::HeightMap) & 1)
			{
				if (!((heightMapInit >> i) & 1))
				{
					std::shared_ptr<HeightMap> heightMap = std::shared_ptr<HeightMap>(new HeightMap(Assets::assetPath + Assets::heightMapFloor));
					terrainMap = heightMap->GetUnsortedVertices();
					heightMapInit |= (1 << i);
				}
			}

			if ((entities[i]->componentIndex >> Components::Collidable) & 1)
			{
				if ((entities[i]->componentIndex >> Components::Player) & 1)
				{
					int xPos = (int)entities[i]->pX;
					int zPos = (int)entities[i]->pZ;
					//get surrounding value of entities pos

					if (xPos < 0)
					{
						//remove it
						xPos = 0;
					}
					else if (xPos > 511)
					{
						xPos = 511;
					}
					if (zPos < 0)
					{
						zPos = 0;
					}
					else if (zPos > 511)
					{
						zPos = 511;
					}

					if (terrainMap[zPos][xPos].position.y - entities[i]->pY <2.0f)
					{
						//get the terrain positions around the player and interpolate towards the correct value.
						entities[i]->pY = terrainMap[zPos][xPos].position.y;
					}
					else
					{
						if ((entities[i]->keyStates >> PLAYER_KEY_S) & 1)
						{
							entities[i]->pZ += 2.0f * entities[i]->dZ;
							entities[i]->pX += 2.0f *entities[i]->dX;
						}
						else
						{
							entities[i]->pZ -= 2.0f *entities[i]->dZ;
							entities[i]->pX -= 2.0f *entities[i]->dX;
						}
					}
				}

				if ((entities[i]->componentIndex >> Components::Ability_Fireball) & 1)
				{
					int xPos = (int)entities[i]->pX;
					int zPos = (int)entities[i]->pZ;
					//get surrounding value of entities pos
					if (xPos < 0)
					{
						xPos = 0;
					}
					else if (xPos > 511)
					{
						xPos = 511;
					}					
					if (zPos < 0)
					{
						zPos = 0;
					}
					else if (zPos > 511)
					{
						zPos = 511;	
					}


					if (terrainMap[zPos][xPos].position.y - entities[i]->pY < -10.0f)
					{
					}
					else
					{
						glm::vec3 v = glm::vec3(entities[i]->dX, 0.0f, entities[i]->dZ);
						glm::vec3 n = terrainMap[zPos][xPos].normal;

						glm::vec3 reflection = 2.0f * glm::dot(v, n) * n - v;
						
						reflection = glm::normalize(reflection);
						entities[i]->dZ = reflection.z;
						entities[i]->dX = reflection.x;

						if (entities[i]->velocity < 3.0f)
						{
							entities[i]->velocity = entities[i]->velocity * 1.15f;
						}
					}

				}
				
				for (int x = 0; x < entities.size(); x++)
				{
					if (x != i)
					{
						if (entities[x])
						{
							if (entities[i]->componentIndex >> Components::Ability_Fireball & 1)
							{
								if (entities[x]->componentIndex >> Components::Collidable & 1)
								{
									if (entities[x]->componentIndex >> Components::Player & 1)
									{
										Box box1 = GetBox(glm::vec3(entities[i]->bcX, entities[i]->bcY, entities[i]->bcZ));
										box1.position = glm::vec3(entities[i]->pX, entities[i]->pY, entities[i]->pZ);
										//box1.scale = glm::vec3(entities[i]->sX, entities[i]->sY, entities[i]->sZ);
										//box1.rotation = glm::vec3(entities[i]->rX, entities[i]->rY, entities[i]->rZ);

										Box box2 = GetBox(glm::vec3(entities[x]->bcX, entities[x]->bcY, entities[x]->bcZ));
										box2.position = glm::vec3(entities[x]->pX, entities[x]->pY, entities[x]->pZ);
										//box2.scale = glm::vec3(entities[x]->sX, entities[x]->sY, entities[x]->sZ);
										//box2.rotation = glm::vec3(entities[x]->rX, entities[x]->rY, entities[x]->rZ);

										if (CheckGJK(box1, box2))
										{
											std::cout << "Collision detected! " << std::endl;
											removeRequest.push_back(i);
											//remove fireball

											//Deal damage to enemy
											entities[x]->hitPoints -= 1;
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
}

bool CollisionSystem::CheckGJK( Box &box1, Box &box2)
{
	glm::vec3 direction = glm::vec3(1.0f);
	glm::vec3 a, b, c, d;

	c = Support(box1, box2, direction);

	direction = -c;
	b = Support(box1, box2, direction);
	if (glm::dot(b, direction) < 0)
	{
		return false;
	}

	direction = glm::cross(glm::cross(c - b, -b), c - b);
	int simplexCount = 2;
	for (int i = 0; i < 50; i++)
	{
		a = Support(box1, box2, direction);
		if (glm::dot(a, direction) < 0)
		{
			return false;
		}
		else if (ContainsOrigin(a, b, c, d, direction, simplexCount))
		{
			return true;
		}
	}
	return false;
}

glm::vec3 CollisionSystem::Support(Box& box1, Box& box2, const glm::vec3& direction)
{
	glm::vec3 furthestPointOfBox1 = GetFurthestPointInDir(box1, direction);
	glm::vec3 furthestPointOfBox2 = GetFurthestPointInDir(box2, -direction);

	return furthestPointOfBox1 - furthestPointOfBox2;
}

bool CollisionSystem::ContainsOrigin(glm::vec3& a, glm::vec3& b, glm::vec3& c, glm::vec3& d, glm::vec3& dir, int& simplexCount)
{
	if (simplexCount == 2)
	{
		glm::vec3 ao = glm::vec3(-a.x, -a.y, -a.z);
		glm::vec3 ab = b - a;
		glm::vec3 ac = c - a;
		glm::vec3 abc = glm::cross(ab, ac);

		glm::vec3 ab_abc = glm::cross(ab, abc);
		if (glm::dot(ab_abc, ao) > 0)
		{
			c = b;
			b = a;

			dir = glm::cross(glm::cross(ab, ao), ab);
			return false;
		}

		glm::vec3 abc_ac = glm::cross(abc, ac);

		if (glm::dot(abc_ac, ao) > 0)
		{
			b = a;
			dir = glm::cross(glm::cross(ac, ao), ac);
			return false;
		}

		if (glm::dot(abc, ao) > 0)
		{
			d = c;
			c = b;
			b = a;
			dir = abc;
		}
		else
		{
			d = b;
			b = a;
			dir = -abc;
		}

		simplexCount = 3;

		return false;
	}
	else if (simplexCount == 3)
	{
		glm::vec3 ao = -a;
		glm::vec3 ab = b - a;
		glm::vec3 ac = c - a;
		glm::vec3 abc = glm::cross(ab, ac);

		//case 1
		if (glm::dot(abc, ao) > 0)
		{
			CheckTetrahedron(ao, ab, ac, abc, a, b, c, d, dir, simplexCount);
		}

		//case 2
		glm::vec3 ad = d - a;

		glm::vec3 acd = glm::cross(ac, ad);
		if (glm::dot(acd, ao) > 0)
		{
			b = c;
			c = d;
			ab = ac;
			ac = ad;
			abc = acd;
			CheckTetrahedron(ao, ab, ac, abc, a, b, c, d, dir, simplexCount);
		}

		glm::vec3 adb = glm::cross(ad, ab);

		if (glm::dot(adb, ao) > 0)
		{
			c = b;
			b = d;

			ac = ab;
			ab = ad;

			abc = adb;
			CheckTetrahedron(ao, ab, ac, abc, a,b ,c,d, dir, simplexCount);
		}
		return true;
	}
	return false;
}

bool CollisionSystem::CheckTetrahedron(const glm::vec3 & ao, const glm::vec3 & ab, const glm::vec3 & ac, const glm::vec3 & abc, glm::vec3& a, glm::vec3& b, glm::vec3& c, glm::vec3& d, glm::vec3 direction, int& simplexCount)
{
	glm::vec3 ab_abc = glm::cross(ab, abc);

	if (glm::dot(ab_abc, ao) > 0)
	{
		c = b;
		b = a;
		direction = glm::cross(glm::cross(ab, ao), ab);

		simplexCount = 2;
		return false;
	}

	glm::vec3 acp = glm::cross(abc, ac);

	if (glm::dot(acp, ao) > 0)
	{
		b = a;

		direction = glm::cross(glm::cross(ac, ao), ac);

		simplexCount = 2;

		return false;
	}
	d = c;
	c = b;
	b = a;

	direction = abc;

	simplexCount = 3;

	return false;
}

Box CollisionSystem::GetBox(glm::vec3 size)
{
	Box box;
	box.vertices.push_back(glm::vec3(0.0, 0.0, 0.0));
	box.vertices.push_back(glm::vec3(size.x, 0.0, 0.0));
	box.vertices.push_back(glm::vec3(size.x, size.y, 0.0));
	box.vertices.push_back(glm::vec3(0.0, size.y, 0.0));
	box.vertices.push_back(glm::vec3(0.0, 0.0, size.z));
	box.vertices.push_back(glm::vec3(size.x, 0.0, size.z));
	box.vertices.push_back(glm::vec3(size.x, size.y, size.z));
	box.vertices.push_back(glm::vec3(0.0, size.y, size.z));
	return box;
}

glm::vec3 CollisionSystem::GetFurthestPointInDir(Box & box, glm::vec3 dir)
{
	glm::vec3 returnVector;
	glm::vec3 m = box.position + glm::vec3(box.vertices[0].x, box.vertices[0].y, box.vertices[0].z);
//	m *= box.scale;
//	glm::mat4 rotation = glm::rotate(box.rotation.x, glm::vec3(0.0f, 1.0f, 0.0f));
//	m = glm::vec4(m, 1.0f) * rotation;

	glm::vec3 worldVertex = glm::vec3(m.x, m.y, m.z);

	float maxDot = glm::dot(worldVertex, dir);
	returnVector = worldVertex;

	for (int i = 1; i < box.vertices.size(); i++)
	{
		m = box.position + glm::vec3(box.vertices[i].x, box.vertices[i].y, box.vertices[i].z);
		//m *= box.scale;
		//m = glm::vec4(m, 1.0f) * rotation;
		glm::vec3 worldVertex = glm::vec3(m.x, m.y, m.z);

		float dot = glm::dot(worldVertex, dir);
		if (dot > maxDot)
		{
			maxDot = dot;
			returnVector = worldVertex;
		}
	}
	return returnVector;
}

