#include "InputHandler.h"


	glm::vec2 InputHandler::mousePosition = glm::vec2(0.0f, 0.0f);
	Key InputHandler::keys[256] = { 0 };
	Key InputHandler::mouseButton[3] = { 0 };
	int InputHandler::mouseWheel = 0;

	void InputHandler::ResetInputs()
	{
		for (int i = 0; i < 256; i++)
		{
			keys[i].type = INPUT_RESET;
		}
		mouseWheel = 0;
	}

	void InputHandler::SetMouse(glm::vec2 position, WPARAM wParam)
	{
		mousePosition = position;
		switch (wParam)
		{
		case WM_LBUTTONDOWN:
		{
			keys[0].type = INPUT_KEYDOWN;
			keys[0].isDown = true;
		}break;

		case WM_RBUTTONDOWN:
		{
			keys[1].type = INPUT_KEYDOWN;
			keys[1].isDown = true;
		}break;
		case WM_MBUTTONDOWN:
		{
			keys[2].type = INPUT_KEYDOWN;
			keys[2].isDown = true;
		}break;
		case WM_LBUTTONUP:
		{
			keys[0].type = INPUT_KEYUP;
			keys[0].isDown = true;
		}break;
		case WM_RBUTTONUP:
		{
			keys[1].type = INPUT_KEYUP;
			keys[1].isDown = true;
		}break;
		case WM_MBUTTONUP:
		{
			keys[2].type = INPUT_KEYUP;
			keys[2].isDown = true;
		}break;
		}
	}

	void InputHandler::HandleInput(INPUTTYPE type, int key)
	{
		if (type == INPUT_RESET)
		{
			for (int i = 0; i < 256; i++)
			{
				keys[i].type = INPUT_RESET;
				mouseWheel = 0;
			}
		}
		if (type == INPUT_KEYDOWN)
		{
			keys[key].type = INPUT_KEYDOWN;
			keys[key].isDown = true;
		}
		if (type == INPUT_KEYUP)
		{
			keys[key].type = INPUT_KEYUP;
			keys[key].isDown = false;
		}
		if (type == INPUT_MOUSEWHEEL)
		{
			if (key < 0)
			{
				mouseWheel = -1;
			}
			else if (key > 0)
			{
				mouseWheel = 1;
			}
		}
	}

	glm::vec2 InputHandler::GetMousePosition()
	{
		return mousePosition;
	}

	bool InputHandler::GetMouseKey(int key)
	{
		return mouseButton[key].isDown;
	}

	bool InputHandler::GetMouseKeyUp(int key)
	{
		if (mouseButton[key].type == INPUT_KEYUP)
		{
			return !mouseButton[key].isDown;
		}
		return false;
	}

	bool InputHandler::GetMouseKeyDown(int key)
	{
		if (mouseButton[key].type == INPUT_KEYDOWN)
		{
			return mouseButton[key].isDown;
		}
		return false;
	}

	bool InputHandler::GetKey(int key)
	{
		return keys[key].isDown;
	}

	bool InputHandler::GetKeyUp(int key)
	{
		if (keys[key].type == INPUT_KEYUP)
		{
			return !keys[key].isDown;
		}
		return false;
	}

	bool InputHandler::GetKeyDown(int key)
	{
		if (keys[key].type == INPUT_KEYDOWN)
		{
			return keys[key].isDown;
		}
		return false;
	}

	int InputHandler::GetMouseWheel()
	{
		return mouseWheel;
	}
