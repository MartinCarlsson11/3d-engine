#pragma once
#include "glm\glm.hpp"
#include "WindowsHeaderIsBullshit.h"

enum INPUTTYPE
{
	INPUT_KEYDOWN,
	INPUT_KEYUP,
	INPUT_MOUSEMOVE,
	INPUT_MOUSEWHEEL,
	INPUT_RESET
};

enum
{
	KEY_A = 0x41, KEY_B, KEY_C,
	KEY_D, KEY_E, KEY_F,
	KEY_G, KEY_H, KEY_I,
	KEY_J, KEY_K,KEY_L, 
	KEY_M, KEY_N,KEY_O,
	KEY_P, KEY_Q,KEY_R, 
	KEY_S, KEY_T, KEY_U, 
	KEY_V, KEY_W, KEY_X, 
	KEY_Y, KEY_Z,
};

enum
{
	KEY_0 = 0x30, KEY_1, 
	KEY_2, KEY_3,
	KEY_4, KEY_5, KEY_6,
	KEY_7, KEY_8, KEY_9
};

struct Key
{
	int keyId;
	INPUTTYPE type;
	bool isDown;
};

class InputHandler
{
public:
	static void HandleInput(INPUTTYPE type, int key);
	static void ResetInputs();
	static void SetMouse(glm::vec2 position, WPARAM wparam);
	static glm::vec2 GetMousePosition();

	static bool GetMouseKey(int key);
	static bool GetMouseKeyUp(int key);
	static bool GetMouseKeyDown(int key);

	static bool GetKey(int key);
	static bool GetKeyUp(int key);
	static bool GetKeyDown(int key);

	static int GetMouseWheel();

	static glm::vec2 mousePosition;
	static Key mouseButton[3];
	static Key keys[256];
	static int mouseWheel;
};

