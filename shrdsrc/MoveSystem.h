
#include "Entity.h"
#include <memory>
#include <vector>
struct MoveSystem
{
	void Update(std::vector<std::shared_ptr<Entity>> entities);
};
