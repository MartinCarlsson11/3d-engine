#include "StateMachine.h"

StateMachine::StateMachine(IState* entryState):
state(nullptr)
{
	state = entryState;
}

StateMachine::~StateMachine(){
	if (state != nullptr){
		delete state;
		state = nullptr;
	}
}

void StateMachine::Start(){
	state->Start();
}

bool StateMachine::Update(){
	if (state != nullptr)
		if (!state->Update())
			return NextState();
	return true;
}

bool StateMachine::NextState(){
	IState* nextState = state->NextState();

	if (state != nullptr){
		delete state;
		state = nullptr;
	}

	if (nextState != nullptr){
		state = nextState;
		nextState = nullptr;
	}
	
	if (state == nullptr){
		return false;
	}
	return true;
}
