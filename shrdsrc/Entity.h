#pragma once

const int MAXENTITIES = 12 + 1;

#include "glm\glm.hpp"

enum PlayerKeys
{
	PLAYER_KEY_W,
	PLAYER_KEY_A,
	PLAYER_KEY_S,
	PLAYER_KEY_D,
	PLAYER_KEY_1,
	PLAYER_KEY_2,
	PLAYER_KEY_3,
	PLAYER_KEY_4,
	PLAYER_KEY_5,
	PLAYER_KEY_6,
	PLAYER_KEY_SPACE,
	PLAYER_KEY_Q,
	PLAYER_KEY_E,
	PLAYER_LMOUSE,
};

static const int
IN_KEY_W = 1 << PLAYER_KEY_W,
IN_KEY_A = 1 << PLAYER_KEY_A,
IN_KEY_S = 1 << PLAYER_KEY_S,
IN_KEY_D = 1 << PLAYER_KEY_D,
IN_KEY_1 = 1 << PLAYER_KEY_1,
IN_KEY_2 = 1 << PLAYER_KEY_2,
IN_KEY_3 = 1 << PLAYER_KEY_3,
IN_KEY_4 = 1 << PLAYER_KEY_4,
IN_KEY_5 = 1 << PLAYER_KEY_5,
IN_KEY_6 = 1 << PLAYER_KEY_6,
IN_KEY_SPACE = 1 << PLAYER_KEY_SPACE,
IN_KEY_Q = 1 << PLAYER_KEY_Q,
IN_KEY_E = 1 << PLAYER_KEY_E,
IN_LMOUSE = 1 << PLAYER_LMOUSE;


namespace Components
{
	enum
	{
		Move,
		Jump,
		Attack,
		Player,
		Renderable,
		Ai,
		Light,
		Fall,
		Health,
		Collidable,
		HeightMap,
		Ability_Fireball,
		Ability_
	};

	const int
		MOVEABLE = 1 << Move,
		JUMP = 1 << Jump,
		ATTACK = 1 << Attack,
		PLAYER = 1 << Player,
		RENDERABLE = 1 << Renderable,
		LIGHT = 1 << Light,
		COLLIDABLE = 1 << Collidable,
		HEIGHTMAP = 1 << HeightMap,
		ABILITYFIREBALL = 1 << Ability_Fireball,
		AI = 1 << Ai;
}

struct Entity
{
	unsigned int componentIndex;

	float pX, pY, pZ, sX, sY, sZ, rX, rY, rZ;
	float dX, dY, dZ, velocity;
	unsigned int keyStates;
	unsigned int entityState;
	unsigned int lightType;
	unsigned int lightId;
	unsigned int meshID;
	unsigned int renderId;

	int hitPoints;
	int hasFired;
	float fireBallCooldown;
	int bounceCount;
	//Box collider size
	//Box collier offset from pX, pY,pZ
	float bcX, bcY, bcZ, bcXOffset, bcYOffset, bcZOffset;
};