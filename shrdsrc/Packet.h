#pragma once
#include <vector>
#include "Address.h"

namespace PacketType
{
	enum : char {
		INPUT = '0',
		JOIN,
		DISCONNECT,
		GAMESTATE,
		REMOVEENTITY,
		ACCEPT,
		DENY,
		OBSERVER
	};
}


class Packet{
public:
	Packet(std::vector<char> data, Address receiver);
	std::vector<char> SendPacket()const;
	Address GetAddress();
private:
	Address receiver;
	std::vector<char> data;
};

