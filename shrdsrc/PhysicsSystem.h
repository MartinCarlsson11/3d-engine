#pragma once
#include "Entity.h"
#include <vector>
#include <memory>

struct PhysicsSystem
{
	void Update(std::vector<std::shared_ptr<Entity>> entities);

	const float gravity = 9.8f;
};
