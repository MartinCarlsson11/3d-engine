#pragma once
#include "Entity.h"
#include <memory>
#include <vector>
struct PlayerSystem
{
	void Update(std::vector<std::shared_ptr<Entity>> entities, std::vector<Entity> &requestCreate);
};