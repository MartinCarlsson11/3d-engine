#include "Packet.h"
#include <cassert>
#include <sstream>

Packet::Packet(std::vector<char> data, Address receiver)
{
	this->receiver = receiver;
	this->data = data;
}

std::vector<char> Packet::SendPacket() const{
	return data;	
}

Address Packet::GetAddress(){
	return receiver;
}