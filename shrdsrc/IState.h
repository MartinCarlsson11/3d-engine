#pragma once
#include <vector>
#include <utility>
#include "Packet.h"
class IState {
public:
	virtual void Start() {}
	virtual bool Update() { return false; }
	virtual void Input(std::vector<char> inputs, Address sender){}
	virtual IState* NextState() { return nullptr; }
};