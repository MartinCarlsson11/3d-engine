#pragma once
#include "IState.h"
#include <vector>
#include <utility>
class StateMachine
{
public:
	StateMachine(IState* entryState);
	~StateMachine();
	void Start();
	bool Update();
private:
	bool NextState();
	IState*state;
};

