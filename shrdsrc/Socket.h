#pragma once
#include "Address.h"
#include <vector>

const unsigned int _CLIENT = 0;
const unsigned int _SERVER = 1;

class Socket
{
public:
	Socket(unsigned int socketType);
	bool Open(unsigned short port);
	void Close();
	bool Send(Address destination, std::vector<char> data);
	int Receive(Address& sender,std::vector<char>& data);
private:
	unsigned int socketType;
	unsigned int handle;
};

