#pragma once
#include <WinSock2.h>
#include <WS2tcpip.h>
#pragma comment(lib, "Ws2_32.lib")
#include "IState.h"
#include <iostream>
#include <vector>
#include "Socket.h"
#include "Packet.h"
#include "Address.h"
#include <utility>
#define SERVER_PORT 27016

class NetworkManager
{
public:
	NetworkManager(unsigned int socketType);
	~NetworkManager();
	void Update(IState* state);
	void AddPacket(Packet& packet);
private:
	Socket* socket;
	std::vector<Address> clients;
	std::vector<Packet> sendPackets;
};

